package sfaxx.refinedtweaks.client;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import sfaxx.refinedtweaks.init.RTBlocks;

public class RTCreativeTab extends CreativeTabs
{
    private static RTCreativeTab rtTab;

    public RTCreativeTab()
    {
        super("Refined Tweaks");
    }

    @Override
    public String getTranslatedTabLabel()
    {
        return super.getTabLabel();
    }

    @Override
    public Item getTabIconItem()
    {
        return Item.getItemFromBlock(RTBlocks.XP_STORAGE.block);
    }

    public static RTCreativeTab getTab()
    {
        if(rtTab == null)
        {
            rtTab = new RTCreativeTab();
        }

        return rtTab;
    }
}
