package sfaxx.refinedtweaks.client.gui;

import java.util.List;

import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.resources.I18n;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import sfaxx.refinedtweaks.basics.BasicGuiContainer;
import sfaxx.refinedtweaks.blocks.tileentities.TileDisenchanter;
import sfaxx.refinedtweaks.inventory.ContainerDisenchanter;
import sfaxx.refinedtweaks.references.Names;
import sfaxx.refinedtweaks.util.ItemStackUtil;

public class GuiDisenchanter extends BasicGuiContainer<TileDisenchanter>
{
	//TODO: Add NORMAL GUI texture
	public static final ResourceLocation background = new ResourceLocation(Names.Textures.GUI.DISENCHANTER);
	
	private ItemStack enchanted;
	
	public GuiDisenchanter(EntityPlayer player, TileDisenchanter disenchanter)
	{
		super(new ContainerDisenchanter(player, disenchanter), disenchanter, 176, 133);
	}
	
	@Override
	public void initGui()
	{
		super.initGui();
		
		enchanted = ItemStackUtil.getRandomEnchantableItem();
	}

	@Override
	protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY)
	{
		fontRendererObj.drawString(I18n.format("container.inventory"), 8, ySize - 110 + 2, 4210752);
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY)
	{
		GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
		mc.getTextureManager().bindTexture(background);
		drawTexturedModalRect(left, top, 0, 0, guiWidth, guiHeight);
		
		int progress = (int) ((this.te.getProgress() / 60f) * 12);
		drawTexturedModalRect(left + 89, top + 23, 177, 0, progress, 10);	

		List<Slot> slotList = inventorySlots.inventorySlots;
		for(Slot slot : slotList)
		{
			if(0 == slot.getSlotIndex() && null == slot.getStack()) 
			{
				ItemStack stack = enchanted.copy();
				stack.addEnchantment(Enchantment.getEnchantmentByID(1), 1);
				ItemStackUtil.renderGhostItemStack(mc, stack, left + 32, top + 20);
			}
			if(1 == slot.getSlotIndex() && null == slot.getStack()) 
			{
				ItemStackUtil.renderGhostItemStack(mc, new ItemStack(Items.BOOK), left + 68, top + 20);
			}
			if(2 == slot.getSlotIndex() && null == slot.getStack()) 
			{
				ItemStackUtil.renderGhostItemStack(mc, new ItemStack(Items.ENCHANTED_BOOK), left + 104, top + 20);
			}
			if(3 == slot.getSlotIndex() && null == slot.getStack()) 
			{
				ItemStackUtil.renderGhostItemStack(mc, enchanted, left + 128, top + 20);
			}
		}
	}

}
