package sfaxx.refinedtweaks.client.gui.components;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.ResourceLocation;

public class GuiSpriteButton extends GuiButton
{
	private ResourceLocation background;
	
	protected int u;
	protected int v;
	
	public GuiSpriteButton(int id, int x, int y, int u, int v, ResourceLocation background)
	{
		super(id, x, y, 20, 20, "");
		this.u = u;
		this.v = v;
		this.background = background;
	}
	
	@Override
	public void drawButtonForegroundLayer(int mouseX, int mouseY)
	{
		
	}
	
	@Override
	public void drawButton(Minecraft mc, int mouseX, int mouseY)
	{
		hovered = mouseX >= xPosition && mouseX < xPosition + width && mouseY >= yPosition && mouseY < yPosition + height;
		mc.renderEngine.bindTexture(background);
		GlStateManager.color(1F, 1F, 1F, 1F);
		
		if(this.enabled)
		{
			if(hovered)
			{
				drawTexturedModalRect(xPosition, yPosition, u, v + 20, 20, 20);
				return;
			}
			
			drawTexturedModalRect(xPosition, yPosition, u, v, 20, 20);
		}
		else
		{
			drawTexturedModalRect(xPosition, yPosition, u, v + 40, 20, 20);
		}
	}
}
