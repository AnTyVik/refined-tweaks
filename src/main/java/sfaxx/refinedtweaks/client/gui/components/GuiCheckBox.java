package sfaxx.refinedtweaks.client.gui.components;

import java.util.ArrayList;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.ResourceLocation;

public class GuiCheckBox extends GuiButton
{
	private ResourceLocation background;
	
	protected int u;
	protected int v;
	
	protected ArrayList<String> tooltip;
	
	protected boolean isChecked;

	public GuiCheckBox(int id, int x, int y, int u, int v, ResourceLocation background, boolean isChecked)
	{
		this(id, x, y, u, v, background, isChecked, new ArrayList<String>());
	}
	
	public GuiCheckBox(int id, int x, int y, int u, int v, ResourceLocation background, boolean isChecked, ArrayList<String> tooltip)
	{
		super(id, x, y, 8, 8, "");
		this.u = u;
		this.v = v;
		this.background = background;
		this.isChecked = isChecked;
		this.tooltip = tooltip;
	}

	@Override
	public void drawButton(Minecraft mc, int mouseX, int mouseY)
	{
		hovered = mouseX >= xPosition && mouseX < xPosition + width && mouseY >= yPosition && mouseY < yPosition + height;
		mc.renderEngine.bindTexture(background);
		GlStateManager.color(1F, 1F, 1F, 1F);
		
		if(this.enabled)
		{
			if(isChecked)
			{
				drawTexturedModalRect(xPosition, yPosition, u, v + 8, 8, 8);
			}
			else
			{
				drawTexturedModalRect(xPosition, yPosition, u, v, 8, 8);
			}
			
			if(!tooltip.isEmpty())
			{
				//draw tooltip
			}
		}
		else
		{
			drawTexturedModalRect(xPosition, yPosition, u, v + 16, 8, 8);
		}
	}
	
	/**
	 * @return new value of <i>isChecked</i>
	 **/
	public boolean toggle()
	{
		isChecked = !isChecked;
		return isChecked;
	}
	
	public boolean isChecked()
	{
		return isChecked;
	}
}
