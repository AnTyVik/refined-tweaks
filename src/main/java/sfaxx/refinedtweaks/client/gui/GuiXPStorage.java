package sfaxx.refinedtweaks.client.gui;

import java.io.IOException;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.util.ResourceLocation;
import sfaxx.refinedtweaks.basics.BasicGuiContainer;
import sfaxx.refinedtweaks.blocks.tileentities.TileXPStorage;
import sfaxx.refinedtweaks.client.gui.components.GuiSpriteButton;
import sfaxx.refinedtweaks.inventory.ContainerXPStorage;
import sfaxx.refinedtweaks.network.MessageXpStorage;
import sfaxx.refinedtweaks.network.RTPacketHandler;
import sfaxx.refinedtweaks.references.Names;

public class GuiXPStorage extends BasicGuiContainer<TileXPStorage>
{
	public static final ResourceLocation background = new ResourceLocation(Names.Textures.GUI.XP_STORAGE);
	
	public GuiXPStorage(TileXPStorage xpStorage)
	{
		super(new ContainerXPStorage(xpStorage), xpStorage, 176, 90);
	}
	
	@Override
	public void addButtons()
	{
		buttonList.clear();
		
		buttonList.add(new GuiSpriteButton(1, left + 45, top + 13, 176, 0, background));
		buttonList.add(new GuiSpriteButton(2, left + 77, top + 13, 196, 0, background));
		buttonList.add(new GuiSpriteButton(3, left + 109, top + 13, 216, 0, background));
		buttonList.add(new GuiSpriteButton(-1, left + 45, top + 58, 176, 60, background));
		buttonList.add(new GuiSpriteButton(-2, left + 77, top + 58, 196, 60, background));
		buttonList.add(new GuiSpriteButton(-3, left + 109, top + 58, 216, 60, background));
	}
	
	@Override
	protected void actionPerformed(GuiButton button) throws IOException
	{
		super.actionPerformed(button);
		
		if((button.id > 0 && button.id <= 3) || (button.id < 0 && button.id >= -3))
		{
			RTPacketHandler.INSTANCE.sendToServer(new MessageXpStorage(te, button.id));
		}
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY)
	{
		int level = te.getLevel();
		int bar = (int) (101 * te.getBarXP());
		
		mc.renderEngine.bindTexture(background);
		drawTexturedModalRect(left, top, 0, 0, guiWidth, guiHeight);
		
		drawTexturedModalRect(left + 37, top + 46, 0, 90, 101, 5);
		drawTexturedModalRect(left + 37, top + 46, 0, 95, bar, 5);
		String l = "" + level;

		int lx = left + (guiWidth / 2 - fontRendererObj.getStringWidth(l) / 2);
		int ly = top + 37;
		fontRendererObj.drawString(l, lx, ly + 1, 0, false);
		fontRendererObj.drawString(l, lx, ly - 1, 0, false);
		fontRendererObj.drawString(l, lx + 1, ly, 0, false);
		fontRendererObj.drawString(l, lx - 1, ly, 0, false);
		fontRendererObj.drawString(l, lx, ly, 8453920, false);
	}
}
