package sfaxx.refinedtweaks.client.gui.config;

import net.minecraft.client.gui.GuiScreen;
import net.minecraftforge.fml.client.config.GuiConfig;
import sfaxx.refinedtweaks.handler.RTConfig;
import sfaxx.refinedtweaks.references.Links;

public class RTGuiConfig extends GuiConfig
{
	public RTGuiConfig(GuiScreen parentScreen)
	{
		super(parentScreen, RTConfig.getInstance().getConfigElements(), Links.MODID, false, false, GuiConfig.getAbridgedConfigPath(RTConfig.getInstance().getConfig().toString()));
	}
	
}
