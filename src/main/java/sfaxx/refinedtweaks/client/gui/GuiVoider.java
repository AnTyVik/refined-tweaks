package sfaxx.refinedtweaks.client.gui;

import java.io.IOException;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;
import sfaxx.refinedtweaks.basics.BasicGuiContainer;
import sfaxx.refinedtweaks.blocks.tileentities.TileVoider;
import sfaxx.refinedtweaks.client.gui.components.GuiCheckBox;
import sfaxx.refinedtweaks.client.gui.components.GuiSpriteButton;
import sfaxx.refinedtweaks.inventory.ContainerVoider;
import sfaxx.refinedtweaks.network.MessageVoider;
import sfaxx.refinedtweaks.network.RTPacketHandler;
import sfaxx.refinedtweaks.references.Names;
import sfaxx.refinedtweaks.util.GuiUtils;

public class GuiVoider extends BasicGuiContainer<TileVoider>
{
	public static final ResourceLocation background = new ResourceLocation(Names.Textures.GUI.VOIDER);

	public GuiVoider(EntityPlayer player, TileVoider voider)
	{
		super(new ContainerVoider(player, voider), voider, 205, 209);
	}
	
	@Override
	protected void addButtons()
	{
		buttonList.clear();
		
		buttonList.add(new GuiSpriteButton(0, left + 33, top + 16, 218, 0, background));
		buttonList.add(new GuiSpriteButton(1, left + 138, top + 16, 218, 0, background));
		buttonList.add(new GuiCheckBox(2, left + 71, top + 28, 238, 0, background, te.canConnectForItems()));
		buttonList.add(new GuiCheckBox(3, left + 191, top + 28, 238, 0, background, te.canConnectForFluids()));
	}
	
	@Override
	protected void actionPerformed(GuiButton button) throws IOException
	{
		super.actionPerformed(button);
		
		if(0 == button.id)
		{
			RTPacketHandler.INSTANCE.sendToServer(new MessageVoider(te, 0, false));
		}
		else if(1 == button.id)
		{
			RTPacketHandler.INSTANCE.sendToServer(new MessageVoider(te, 1, false));
		}
		else if(2 == button.id)
		{
			RTPacketHandler.INSTANCE.sendToServer(new MessageVoider(te, 2, ( (GuiCheckBox) button).toggle()));
		}
		else if(3 == button.id)
		{
			RTPacketHandler.INSTANCE.sendToServer(new MessageVoider(te, 3, ( (GuiCheckBox) button).toggle()));
		}
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY)
	{
		mc.renderEngine.bindTexture(background);
		drawTexturedModalRect(left, top, 0, 0, guiWidth, guiHeight);
		
		if(null != te.tank.getFluid() && null != te.tank.getFluid().getFluid())
		{
			float h = (float) te.tank.getFluidAmount() / (float) te.tank.getCapacity();
			GuiUtils.drawFluidInGui(te.tank.getFluid().getFluid(), left + 140, top + 113 - 69 * h, 16f, 69 * h);
		}
		
		mc.renderEngine.bindTexture(background);
		drawTexturedModalRect(left + 140, top + 50, 206, 0, 12, 57);
	}

	@Override
	public void onGuiClosed()
	{
		RTPacketHandler.INSTANCE.sendToServer(new MessageVoider(te, 0, false));
		RTPacketHandler.INSTANCE.sendToServer(new MessageVoider(te, 1, false));
		super.onGuiClosed();
	}
}
