package sfaxx.refinedtweaks.handler;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;
import sfaxx.refinedtweaks.blocks.tileentities.TileDisenchanter;
import sfaxx.refinedtweaks.blocks.tileentities.TileVoider;
import sfaxx.refinedtweaks.blocks.tileentities.TileXPStorage;
import sfaxx.refinedtweaks.client.gui.GuiDisenchanter;
import sfaxx.refinedtweaks.client.gui.GuiVoider;
import sfaxx.refinedtweaks.client.gui.GuiXPStorage;
import sfaxx.refinedtweaks.inventory.ContainerDisenchanter;
import sfaxx.refinedtweaks.inventory.ContainerVoider;
import sfaxx.refinedtweaks.inventory.ContainerXPStorage;
import sfaxx.refinedtweaks.references.GuiIDs;

public class RTGuiHandler implements IGuiHandler
{

	@Override
	public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z)
	{
		switch (ID)
		{
			case GuiIDs.XP_STORAGE:
			{
				return new ContainerXPStorage((TileXPStorage) world.getTileEntity(new BlockPos(x, y, z)));
			}
			case GuiIDs.DISENCHANTER:
			{
				return new ContainerDisenchanter(player, (TileDisenchanter) world.getTileEntity(new BlockPos(x, y, z)));
			}
			case GuiIDs.VOIDER:
			{
				return new ContainerVoider(player, (TileVoider) world.getTileEntity(new BlockPos(x, y, z)));
			}
//			case :
//			{
//				return;
//			}
			default:
			{
				return null;
			}
		}
	}

	@Override
	public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z)
	{
		switch (ID)
		{
			case GuiIDs.XP_STORAGE:
			{
				return new GuiXPStorage((TileXPStorage) world.getTileEntity(new BlockPos(x, y, z)));
			}
			case GuiIDs.DISENCHANTER:
			{
				return new GuiDisenchanter(player, (TileDisenchanter) world.getTileEntity(new BlockPos(x, y, z)));
			}
			case GuiIDs.VOIDER:
			{
				return new GuiVoider(player, (TileVoider) world.getTileEntity(new BlockPos(x, y, z)));
			}
//			case :
//			{
//				return;
//			}
			default:
			{
				return null;
			}
		}
	}

}
