package sfaxx.refinedtweaks.handler;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.config.ConfigElement;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.client.config.IConfigElement;
import sfaxx.refinedtweaks.references.Configs;

public class RTConfig
{
	private static RTConfig instance;
	private Configuration config;

	/// Category General
	public boolean changeRSRecipes;
	public boolean changeSJRecipes;
	public boolean changeRFToolsRecipes;
	public boolean changeRFToolsControlRecipes;
	public boolean changeThermalExpansionRecipes;
	
	/// Category Blocks
	public boolean xpStorage;
	public boolean charcoal;
	public boolean pressurePlate;
	public boolean disenchanter;
	public boolean voider;
	
	public static RTConfig getInstance()
	{
		return instance;
	}
	
	public RTConfig(File configFile)
	{
		this.config = new Configuration(configFile);
		MinecraftForge.EVENT_BUS.register(this);
		load();
	}
	
	public static void initialize(File configFile)
	{
		instance = new RTConfig(configFile);
	}
	
	public Configuration getConfig()
	{
		return config;
	}
	
	private void load()
	{
		/// Category General
		changeRSRecipes = config.getBoolean("changeRSRecipes", Configuration.CATEGORY_GENERAL, true, "If true will replace all Quartz Enriched Iron to Steel (if steel is avalable)"/*.replaceAll("#@", "\u0027")*/);
		changeSJRecipes = config.getBoolean("changeSimplyJetpacksRecipes", Configuration.CATEGORY_GENERAL, true, "If true will change Simply Jetpacks 2 recipe for Vanilla Jetpacks (Require Vanilla Jetpacks to be enabled)");
		changeRFToolsRecipes = config.getBoolean("changeRFToolsRecipes", Configuration.CATEGORY_GENERAL, true, "If true will change RF Tools recipes");
		changeRFToolsControlRecipes = config.getBoolean("changeRFToolsControlRecipes", Configuration.CATEGORY_GENERAL, true, "If true will change RF Tools Control recipes");
		changeThermalExpansionRecipes = config.getBoolean("changeThermalExpansionRecipes", Configuration.CATEGORY_GENERAL, true, "If true will change Thermal Expansion recipes");
		
		/// Category Blocks
		xpStorage = config.getBoolean("xpStorage", Configs.CATEGORY_BLOCKS, true, "If false disables block");
		charcoal = config.getBoolean("charcoal", Configs.CATEGORY_BLOCKS, true, "If false disables block");
		pressurePlate = config.getBoolean("pressurePlate", Configs.CATEGORY_BLOCKS, true, "If false disables block");
		disenchanter = config.getBoolean("disenchanter", Configs.CATEGORY_BLOCKS, true, "If false disables block");
		voider = config.getBoolean("voider", Configs.CATEGORY_BLOCKS, true, "If false disables block");
		
        if (config.hasChanged()) 
        {
            config.save();
        }
	}
	
    public List<IConfigElement> getConfigElements() 
    {
        List<IConfigElement> list = new ArrayList<IConfigElement>();

        list.addAll(new ConfigElement(config.getCategory(Configuration.CATEGORY_GENERAL).setRequiresMcRestart(true)).getChildElements());
        list.addAll(new ConfigElement(config.getCategory(Configs.CATEGORY_BLOCKS).setRequiresMcRestart(true)).getChildElements());

        return list;
    }
}
