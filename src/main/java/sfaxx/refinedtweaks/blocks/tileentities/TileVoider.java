package sfaxx.refinedtweaks.blocks.tileentities;

import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.ItemStackHandler;
import sfaxx.refinedtweaks.basics.BasicTileEntity;
import sfaxx.refinedtweaks.inventory.InventoryItemsHandler;

public class TileVoider extends BasicTileEntity
{
	public static final String TAG_BUCKETS = "bucks";
	public static final String TAG_CONNECT_ITEMS = "conI";
	public static final String TAG_CONNECT_FLUIDS = "conF";

	public ItemStackHandler inv;
	public InventoryItemsHandler invBuckets;
	private InventoryVoid invCap = new InventoryVoid();

	public FluidTank tank;
	private TankVoid tankCap;
	
	private boolean canConnectForItems;
	private boolean canConnectForFluids;

	public TileVoider()
	{
		tank = new FluidTank(10 * Fluid.BUCKET_VOLUME);
		tankCap = new TankVoid();
		
		inv = new ItemStackHandler(16);
		invBuckets = new InventoryItemsHandler(4)
		{
			@Override
			protected boolean isStackValidForSlot(ItemStack stack, int slot)
			{
				if(0 == slot)
				{
					if(null == stack)
					{
						return false;
					}
					
					IFluidHandler fluidHandler = stack.getCapability(CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY, null);
					
					if (null != fluidHandler && fluidHandler.getTankProperties()[0].getCapacity() > 0)
					{
						return true;
					}
					
					return Items.WATER_BUCKET == stack.getItem() || Items.LAVA_BUCKET == stack.getItem();
				}
				else if(1 == slot)
				{
					if(null == stack)
					{
						return false;
					}
					
					IFluidHandler fluidHandler = stack.getCapability(CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY, null);
					
					if (null != fluidHandler)
					{
						return true;
					}
					
					return Items.BUCKET == stack.getItem();
				}

				return super.isStackValidForSlot(stack, slot);
			}
			
			@Override
			protected void onContentsChanged(int slot)
			{
				ItemStack stack = getStackInSlot(slot);
				if(null == stack)
				{
					return;
				}
				
				if(0 == slot)
				{
					IFluidHandler fluidHandler = stack.getCapability(CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY, null);
					int freeSpace = tank.getCapacity() - tank.getFluidAmount();
					
					if(null == stack.getItem() || null == fluidHandler)
					{
						return;
					}
					
					FluidStack drain = fluidHandler.drain(freeSpace, false);
					
					if(null == drain || null == drain.getFluid() || (null != tank.getFluid() && drain.getFluid() != tank.getFluid().getFluid()) || 0 >= drain.amount)
					{
						return;
					}
					
					fluidHandler.drain(drain, true);
					tank.fill(drain, true);
					
					ItemStack remaining = insertItem(2, extractItem(slot, getStackInSlot(slot).stackSize, true), true);
					
					if(null == remaining)
					{
						insertItem(2, extractItem(slot, getStackInSlot(slot).stackSize, false), false);
					}
				}
				else if(1 == slot)
				{
					IFluidHandler fluidHandler = stack.getCapability(CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY, null);
					
					if(null == stack.getItem() || null == tank.getFluid() || null == tank.getFluid().getFluid() || 0 >= tank.getFluidAmount() || null == fluidHandler)
					{
						return;
					}
					
					int fill = fluidHandler.fill(tank.getFluid(), false);
					
					if(0 >= fill)
					{
						return;
					}
					
					fluidHandler.fill(tank.getFluid(), true);
					tank.drain(fill, true);

					ItemStack remaining = insertItem(3, extractItem(slot, getStackInSlot(slot).stackSize, true), true);
					
					if(null == remaining)
					{
						insertItem(3, extractItem(slot, getStackInSlot(slot).stackSize, false), false);
					}
				}
			}
		};
		
		this.canConnectForItems = this.canConnectForFluids = false;
	}

	@Override
	public NBTTagCompound writeCustomNBT(NBTTagCompound compound)
	{
		compound.setTag(TAG_BUCKETS, invBuckets.serializeNBT());
		compound.setBoolean(TAG_CONNECT_ITEMS, canConnectForItems);
		compound.setBoolean(TAG_CONNECT_FLUIDS, canConnectForFluids);
		return compound;
	}

	@Override
	public void readCustomNBT(NBTTagCompound compound)
	{
		invBuckets.deserializeNBT(compound.getCompoundTag(TAG_BUCKETS));
		canConnectForItems = compound.getBoolean(TAG_CONNECT_ITEMS);
		canConnectForFluids = compound.getBoolean(TAG_CONNECT_FLUIDS);
	}
	
	@Override
	public NBTTagCompound writeSyncNBT(NBTTagCompound compound)
	{
		compound.setTag("voidI", inv.serializeNBT());
		NBTTagCompound tankCompound = new NBTTagCompound();
		tank.writeToNBT(tankCompound);
		compound.setTag("voidT", tankCompound);
				
		return compound;
	}
	
	@Override
	public void readSyncNBT(NBTTagCompound compound)
	{
		inv.deserializeNBT(compound.getCompoundTag("voidI"));
		NBTTagCompound tankCompound = compound.getCompoundTag("voidT");
		tank.readFromNBT(tankCompound);
	}

	public boolean canConnectForItems()
	{
		return canConnectForItems;
	}
	
	public void setCanConnectForItems(boolean canConnectForItems)
	{
		this.canConnectForItems = canConnectForItems;
//		this.markDirty();
        this.updateContainingBlock(null);
	}

	public boolean canConnectForFluids()
	{
		return canConnectForFluids;
	}

	public void setCanConnectForFluids(boolean canConnectForFluids)
	{
		this.canConnectForFluids = canConnectForFluids;
        this.updateContainingBlock(null);
	}

	public void clearInventory()
	{
		for(int i = 0; i < 16; ++i)
		{
			if(null != inv.getStackInSlot(i))
			{
				inv.extractItem(i, inv.getStackInSlot(i).stackSize, false);
			}
		}
		
        this.updateContainingBlock(null);
	}

	public void clearTank()
	{
		tank.drain(tank.getCapacity(), true);
		this.markDirty();
        this.updateContainingBlock(null);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T getCapability(Capability<T> capability, EnumFacing facing)
	{
		if (canConnectForItems && CapabilityItemHandler.ITEM_HANDLER_CAPABILITY == capability)
		{
			return (T) invCap;
		}

		if (canConnectForFluids && CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY == capability)
		{
			return (T) tankCap;
		}

		return super.getCapability(capability, facing);
	}

	@Override
	public boolean hasCapability(Capability<?> capability, EnumFacing facing)
	{
		return (canConnectForItems && CapabilityItemHandler.ITEM_HANDLER_CAPABILITY == capability) || (canConnectForFluids && CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY == capability) || super.hasCapability(capability, facing);
	}

	private class InventoryVoid extends ItemStackHandler
	{
		@Override
		public ItemStack insertItem(int slot, ItemStack stack, boolean simulate)
		{
			return null;
		}

		@Override
		public ItemStack extractItem(int slot, int amount, boolean simulate)
		{
			return null;
		}
	}

	private class TankVoid extends FluidTank
	{
		public TankVoid()
		{
			super(0);
		}

		@Override
		public boolean canFill()
		{
			return true;
		}

		@Override
		public boolean canDrain()
		{
			return false;
		}

		@Override
		public int fill(FluidStack resource, boolean doFill)
		{
			return resource.amount;
		}
	}
}
