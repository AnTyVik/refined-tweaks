package sfaxx.refinedtweaks.blocks.tileentities;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentData;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.items.CapabilityItemHandler;
import sfaxx.refinedtweaks.basics.BasicTileEntity;
import sfaxx.refinedtweaks.inventory.InventoryItemsHandler;
import sfaxx.refinedtweaks.inventory.InventoryWrapper;

public class TileDisenchanter extends BasicTileEntity implements ITickable
{
	public static final String TAG_WORKING = "working";
	public static final String TAG_PROGRESS = "progress";
	public static final String TAG_INVENTORY = "inventory";
	
	private boolean isWorking;
	private int progress;
	private InventoryItemsHandler inventory;
	private InventoryWrapper inventoryWrapper;

	public TileDisenchanter()
	{
		isWorking = false;
		progress = 0;
		
		inventory = new InventoryItemsHandler(4)
		{
			@Override
			protected boolean isStackValidForSlot(ItemStack stack, int slot)
			{
				if(0 == slot)
				{
					return stack.hasTagCompound() && stack.getTagCompound().hasKey("ench");
				}
				else if(1 == slot)
				{
					return Items.BOOK == stack.getItem();
				}
				
				return super.isStackValidForSlot(stack, slot);
			}
		};
		
		inventoryWrapper = new InventoryWrapper(inventory, 4, 2);
	}
	
	@Override
	public NBTTagCompound writeCustomNBT(NBTTagCompound compound)
	{
		compound.setBoolean(TAG_WORKING, isWorking);
		compound.setInteger(TAG_PROGRESS, progress);
		compound.setTag(TAG_INVENTORY, inventory.serializeNBT());
		return compound;
	}

	@Override
	public void readCustomNBT(NBTTagCompound compound)
	{
		isWorking = compound.getBoolean(TAG_WORKING);
		progress = compound.getInteger(TAG_PROGRESS);
		inventory.deserializeNBT(compound.getCompoundTag(TAG_INVENTORY));
	}

	public InventoryItemsHandler getInventory()
	{
		return inventory;
	}

	public boolean isWorking()
	{
		return isWorking;
	}

	public int getProgress()
	{
		return progress;
	}

	@Override
	public void update()
	{
		if(!canWork())
		{
			if(isWorking)
			{
				done();
			}
			else
			{
				return;
			}
		}
		else
		{
			if(isWorking)
			{
				progress += 1; //TODO: Add upgrade
				if(progress > 60)
				{
					ItemStack item = inventory.getStackInSlot(0);
					inventory.setStackInSlot(0, null);
					inventory.setStackInSlot(2, removeRandomEnchantment(item));
					inventory.setStackInSlot(3, item);
					ItemStack books = inventory.getStackInSlot(1);
					if(books.stackSize > 1)
					{
						books.stackSize--;
					}
					else
					{
						inventory.setStackInSlot(1, null);
					}
				}
			}
			else
			{
				isWorking = true;
			}
		}
	}
	
	private boolean canWork()
	{
		return null != inventory.getStackInSlot(0)&& null != inventory.getStackInSlot(1) && null == inventory.getStackInSlot(2) && null == inventory.getStackInSlot(3);
	}

    private void done() 
    {
        progress = 0;
        isWorking = false;

        markDirty();
    }

	@SuppressWarnings("unchecked")
	@Override
	public <T> T getCapability(Capability<T> capability, EnumFacing facing)
	{
		if(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY == capability)
		{
			return (T) inventoryWrapper;
		}
		
		return super.getCapability(capability, facing);
	}
	
	@Override
	public boolean hasCapability(Capability<?> capability, EnumFacing facing)
	{
		return CapabilityItemHandler.ITEM_HANDLER_CAPABILITY == capability || super.hasCapability(capability, facing);
	}
	
	private ItemStack removeRandomEnchantment(ItemStack toDisenchant)
	{
		ItemStack enchantedBook = new ItemStack(Items.ENCHANTED_BOOK);
		NBTTagList enchs = toDisenchant.getEnchantmentTagList();
		
		if(null != enchs && enchs.tagCount() > 0)
		{
			int toRemove = this.worldObj.rand.nextInt(enchs.tagCount());
			NBTTagCompound rem = (NBTTagCompound) enchs.removeTag(toRemove);
			if(enchs.tagCount() <= 0)
			{
				toDisenchant.getTagCompound().removeTag("ench");
			}
			Items.ENCHANTED_BOOK.addEnchantment(enchantedBook, new EnchantmentData(Enchantment.getEnchantmentByID(rem.getShort("id")), rem.getShort("lvl")));
		}
		
		return enchantedBook;
	}
}
