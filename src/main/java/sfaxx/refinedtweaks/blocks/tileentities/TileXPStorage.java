package sfaxx.refinedtweaks.blocks.tileentities;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import sfaxx.refinedtweaks.basics.BasicTileEntity;
import sfaxx.refinedtweaks.helper.XPHelper;

public class TileXPStorage extends BasicTileEntity
{
    private int xp;
    private int level;
    private float barXP;

    public static final String TAG_XP = "xpTotal";
    public static final String TAG_LEVEL = "levels";
    public static final String TAG_CUR = "xpCurrant";

    @Override
    public NBTTagCompound writeCustomNBT(NBTTagCompound compound)
    {
        compound.setInteger(TAG_XP, xp);
        compound.setInteger(TAG_LEVEL, level);
        compound.setFloat(TAG_CUR, barXP);

        return compound;
    }

    @Override
    public void readCustomNBT(NBTTagCompound compound)
    {
        xp = compound.getInteger(TAG_XP);
        level = compound.getInteger(TAG_LEVEL);
        barXP = compound.getFloat(TAG_CUR);
    }

    private void addXP(int amount)
    {
        int experience = Math.max(0, this.xp + amount);
        this.xp = experience;
        this.level = XPHelper.getLevelForExperience(experience);
        int expForLevel = XPHelper.getExperienceForLevel(this.level);
        this.barXP = (float) (experience - expForLevel) / (float) XPHelper.getBarCapacity(this.level);
        
        this.markDirty();
        this.updateContainingBlock(null);
    }

    public void getXpFromPlayer(EntityPlayer player, Mode mode)
    {
        int xp = 0;

        switch(mode)
        {
            case One:
            {
                xp = (XPHelper.getPlayerXP(player) - XPHelper.getExperienceForLevel(player.experienceLevel - 1));
                break;
            }
            case Five:
            {            	
            	xp = XPHelper.getPlayerXP(player) - XPHelper.getExperienceForLevel(player.experienceLevel - 5);
                break;
            }
            case All:
            {
                xp = XPHelper.getPlayerXP(player);
            }
        }

        XPHelper.addPlayerXP(player, -xp);
        this.addXP(xp);
    }

    public void transferXpToPlayer(EntityPlayer player, Mode mode)
    {
        int xp = 0;

        switch(mode)
        {
            case One:
            {
                xp = Math.min(player.xpBarCap(), this.xp);
                break;
            }
            case Five:
            {
            	xp = Math.min(XPHelper.getExperienceForLevel(player.experienceLevel + 5) - XPHelper.getPlayerXP(player), this.xp);
                break;
            }
            case All:
            {
                xp = this.xp;
            }
        }

        XPHelper.addPlayerXP(player, xp);
        this.addXP(-xp);
    }

    public int getXp()
    {
        return xp;
    }

    public int getLevel()
    {
        return level;
    }

    public float getBarXP()
    {
        return barXP;
    }

    public enum Mode
    {
        One(1, 2),
        Five(2, 0),
        All(0, 1);

        private int next;
        private int previous;

        Mode(int next, int previous)
        {
            this.next = next;
            this.previous = previous;
        }
        
        public static Mode fromInt(int mode)
		{
			if(2 == Math.abs(mode))
			{
				return Mode.Five;
			}
			else if(3 == Math.abs(mode))
			{
				return Mode.All;
			}

			return Mode.One;
		}

        public Mode getNext()
        {
            return Mode.values()[next];
        }

        public Mode getPrevious()
        {
            return Mode.values()[previous];
        }
    }
}
