package sfaxx.refinedtweaks.blocks;

import net.minecraft.block.material.Material;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntityFurnace;
import net.minecraftforge.fml.common.IFuelHandler;
import net.minecraftforge.fml.common.registry.GameRegistry;
import sfaxx.refinedtweaks.basics.BasicBlock;
import sfaxx.refinedtweaks.handler.RTConfig;

public class BlockOfCharcoal extends BasicBlock implements IFuelHandler
{

	public BlockOfCharcoal(String name)
	{
		super(name, Material.ROCK);
        this.setHardness(3F);
        GameRegistry.registerFuelHandler(this);
	}

	@Override
	public int getBurnTime(ItemStack fuel)
	{
		return fuel.getItem() == Item.getItemFromBlock(this) ? TileEntityFurnace.getItemBurnTime(new ItemStack(Blocks.COAL_BLOCK)) : 0;
	}
    
    @Override
    public boolean canRegister()
    {
    	return RTConfig.getInstance().charcoal;
    }
}
