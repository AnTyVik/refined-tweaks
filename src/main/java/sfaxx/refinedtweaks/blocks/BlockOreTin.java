package sfaxx.refinedtweaks.blocks;

import net.minecraft.block.material.Material;
import sfaxx.refinedtweaks.RefinedTweaks;
import sfaxx.refinedtweaks.basics.BasicBlock;
import sfaxx.refinedtweaks.handler.RTConfig;

public class BlockOreTin extends BasicBlock
{

	public BlockOreTin(String name)
	{
		super(name, Material.ROCK);
	}

	@Override
	public boolean canRegister()
	{
		return RefinedTweaks.simplyjetpacksLoaded && RTConfig.getInstance().changeSJRecipes;
	}

}
