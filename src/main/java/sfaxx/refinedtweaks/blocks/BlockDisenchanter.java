package sfaxx.refinedtweaks.blocks;

import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import sfaxx.refinedtweaks.RefinedTweaks;
import sfaxx.refinedtweaks.basics.BasicBlock;
import sfaxx.refinedtweaks.blocks.tileentities.TileDisenchanter;
import sfaxx.refinedtweaks.handler.RTConfig;
import sfaxx.refinedtweaks.references.GuiIDs;

public class BlockDisenchanter extends BasicBlock implements ITileEntityProvider
{
	public BlockDisenchanter(String name)
	{
		super(name);
	}

	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta)
	{
		return new TileDisenchanter();
	}

	@Override
	public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, ItemStack heldItem, EnumFacing side, float hitX, float hitY, float hitZ)
	{
		if(!world.isRemote && !player.isSneaking())
        {
			player.openGui(RefinedTweaks.instance, GuiIDs.DISENCHANTER, world, pos.getX(), pos.getY(), pos.getZ());
        }

        return true;
	}

	@Override
	public void breakBlock(World worldIn, BlockPos pos, IBlockState state)
	{
		super.breakBlock(worldIn, pos, state);
	}
	
    @Override
    public boolean canRegister()
    {
    	return RTConfig.getInstance().disenchanter;
    }
}
