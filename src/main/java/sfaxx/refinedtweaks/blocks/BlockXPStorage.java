package sfaxx.refinedtweaks.blocks;

import javax.annotation.Nullable;

import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import sfaxx.refinedtweaks.RefinedTweaks;
import sfaxx.refinedtweaks.basics.BasicBlock;
import sfaxx.refinedtweaks.blocks.tileentities.TileXPStorage;
import sfaxx.refinedtweaks.handler.RTConfig;
import sfaxx.refinedtweaks.references.GuiIDs;
import vazkii.botania.common.core.helper.ItemNBTHelper;

public class BlockXPStorage extends BasicBlock implements ITileEntityProvider
{
	private static final AxisAlignedBB AABB = new AxisAlignedBB(0, 0, 0, 1, 0.75, 1);
	
    public BlockXPStorage(String name)
    {
        super(name);
        this.setHardness(3F);
    }

    @Override
    public TileEntity createNewTileEntity(World worldIn, int meta)
    {
        return new TileXPStorage();
    }

    @Override
    public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, @Nullable ItemStack heldItem, EnumFacing side, float hitX, float hitY, float hitZ)
    {
        if(!world.isRemote && !player.isSneaking())
        {
            player.openGui(RefinedTweaks.instance, GuiIDs.XP_STORAGE, world, pos.getX(), pos.getY(), pos.getZ());
        }

        return true;
    }
    
    @Override
    public void harvestBlock(World world, EntityPlayer player, BlockPos pos, IBlockState state, TileEntity te, ItemStack stack)
    {
    	TileXPStorage tile = (TileXPStorage) te;
    	ItemStack itemStack = new ItemStack(this, 1, 0);
    	NBTTagCompound tag = tile.writeCustomNBT(new NBTTagCompound());
		ItemNBTHelper.setCompound(itemStack, "teTagCompound", tag);  
		spawnAsEntity(world, pos, itemStack);
    }
    
    @Override
    public ItemStack getPickBlock(IBlockState state, RayTraceResult target, World world, BlockPos pos, EntityPlayer player)
    {
    	TileXPStorage te = (TileXPStorage) world.getTileEntity(pos);
    	ItemStack stack = new ItemStack(this, 1, 0);
    	
		NBTTagCompound tag = te.writeCustomNBT(new NBTTagCompound());
		ItemNBTHelper.setCompound(stack, "teTagCompound", tag);
    	
		return stack;
    }

    @Override
    public void onBlockPlacedBy(World world, BlockPos pos, IBlockState state, EntityLivingBase placer, ItemStack stack)
    {
    	super.onBlockPlacedBy(world, pos, state, placer, stack);

    	NBTTagCompound tag = ItemNBTHelper.getCompound(stack, "teTagCompound", true);
    	if(null != tag)
    	{
    		( (TileXPStorage) world.getTileEntity(pos)).readCustomNBT(tag);
    	}
    }
    
    @Override
    public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos)
    {
    	return AABB;
    }
    
    @Override
    public boolean isFullyOpaque(IBlockState state)
    {
    	return false;
    }
    
    @Override
    public boolean isOpaqueCube(IBlockState state)
    {
    	return false;
    }
    
    @Override
    public boolean canRegister()
    {
    	return RTConfig.getInstance().xpStorage;
    }
}
