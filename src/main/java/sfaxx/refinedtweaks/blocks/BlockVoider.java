package sfaxx.refinedtweaks.blocks;

import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeModContainer;
import net.minecraftforge.fluids.Fluid;
import sfaxx.refinedtweaks.RefinedTweaks;
import sfaxx.refinedtweaks.basics.BasicBlock;
import sfaxx.refinedtweaks.blocks.tileentities.TileVoider;
import sfaxx.refinedtweaks.handler.RTConfig;
import sfaxx.refinedtweaks.references.GuiIDs;

public class BlockVoider extends BasicBlock implements ITileEntityProvider
{
	public BlockVoider(String name)
	{
		super(name);
	}

	@Override
	public boolean canRegister()
	{
		return RTConfig.getInstance().voider;
	}

	@Override
	public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, ItemStack heldItem, EnumFacing side, float hitX, float hitY, float hitZ)
	{
		if(player.isSneaking())
		{
			return false;
		}
		
		boolean openGui = true; 
		
		if(heldItem != null && EnumHand.MAIN_HAND == hand)
		{
			if(ForgeModContainer.getInstance().universalBucket == heldItem.getItem())
			{
				ForgeModContainer.getInstance().universalBucket.drain(heldItem, Fluid.BUCKET_VOLUME, true);
				openGui = false;
			}
			else if(Items.LAVA_BUCKET == heldItem.getItem() || Items.WATER_BUCKET == heldItem.getItem() || Items.MILK_BUCKET == heldItem.getItem())
			{
				int index = player.inventory.currentItem;
				player.inventory.removeStackFromSlot(index);
				player.inventory.setInventorySlotContents(index, new ItemStack(Items.BUCKET));
				openGui = false;
			}
		}
		
		if(openGui)
		{
			player.openGui(RefinedTweaks.instance, GuiIDs.VOIDER, world, pos.getX(), pos.getY(), pos.getZ());
		}
		
		return true;
	}

	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta)
	{
		return new TileVoider();
	}
}
