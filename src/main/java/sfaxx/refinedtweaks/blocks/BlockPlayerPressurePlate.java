package sfaxx.refinedtweaks.blocks;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.PropertyBool;
import net.minecraft.block.state.BlockStateContainer.Builder;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import sfaxx.refinedtweaks.basics.BasicBlock;
import sfaxx.refinedtweaks.basics.BasicItemBlock;
import sfaxx.refinedtweaks.handler.RTConfig;

//TODO: Add camo? (RMB to apply camo/Shift+RMB to remove)
public class BlockPlayerPressurePlate extends BasicBlock
{
	public static final PropertyBool SILENT = PropertyBool.create("silent");
	public static final PropertyBool ACTIVATED = PropertyBool.create("activated");
	
	private static final AxisAlignedBB PRESSED = new AxisAlignedBB(0.0625, 0.0, 0.0625, 0.9375, 0.03125, 0.9375);
    private static final AxisAlignedBB UNPRESSED = new AxisAlignedBB(0.0625, 0.0, 0.0625, 0.9375, 0.0625, 0.9375);
    private static final AxisAlignedBB CHECKRANGE = new AxisAlignedBB(0.125, 0.0, 0.125, 0.875, 0.25, 0.875);

	public BlockPlayerPressurePlate(String name)
	{
		super(name, Material.ROCK);
        this.setResistance(7.5f);
        this.setTickRandomly(true);
	}
	
	@Override
	public void getSubBlocks(Item item, CreativeTabs tab, List<ItemStack> list)
	{
		list.add(new ItemStack(item, 1, 0));
		list.add(new ItemStack(item, 1, 1));
	}
	
	@Override
	public ItemBlock createItemBlock()
	{
		return new BasicItemBlock(this, true);
	}
	
	@Override
	public Map<Integer, String> getVariants()
	{
		Map<Integer, String> vars = new HashMap<Integer, String>();
    	vars.put(0, "activated=false,silent=false");
    	vars.put(1, "activated=false,silent=true");
    	return vars;
	}
	
	@Override
	protected Builder createBlockStateBuilder()
	{
		Builder builder = super.createBlockStateBuilder();
		builder.add(SILENT).add(ACTIVATED);
		
		return builder;
	}
	
	@Override
	public int getMetaFromState(IBlockState state)
	{
		int meta = ((Boolean)state.getValue(SILENT)).booleanValue() ? 1 : 0;
		meta |= ((Boolean)state.getValue(ACTIVATED)).booleanValue() ? 2 : 0;
		return meta;
	}
	
	@Override
	public IBlockState getStateFromMeta(int meta)
	{
		IBlockState state = createBlockState().getBaseState().withProperty(SILENT, (meta & 1) > 0 ? true : false).withProperty(ACTIVATED, (meta & 2) > 0 ? true : false);
		
		return state;
	}
	
	@Override
	public IBlockState getActualState(IBlockState state, IBlockAccess world, BlockPos pos)
	{
		return state.withProperty(ACTIVATED, isActiveted(state));
	}

    public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos)
    {
        boolean flag = ((Boolean)state.getValue(ACTIVATED)).booleanValue();
        return flag ? PRESSED : UNPRESSED;
    }
	
    @Override
    public AxisAlignedBB getCollisionBoundingBox(IBlockState blockState, World world, BlockPos pos)
    {
    	return NULL_AABB;
    }
    
    @Override
    public int tickRate(World world)
    {
    	return 25;
    }

    public boolean isOpaqueCube(IBlockState state)
    {
        return false;
    }

    public boolean isFullCube(IBlockState state)
    {
        return false;
    }

    public boolean isPassable(IBlockAccess world, BlockPos pos)
    {
        return true;
    }
    
    public boolean canSpawnInBlock()
    {
        return true;
    }
    
    @SuppressWarnings("deprecation")
	@Override
    public boolean canPlaceBlockAt(World world, BlockPos pos)
    {
    	return world.getBlockState(pos.down()).isFullyOpaque();
    }
    
    @Override
    public void neighborChanged(IBlockState state, World world, BlockPos pos, Block blockIn)
    {
        if (!this.canPlaceBlockAt(world, pos.down()))
        {
            this.dropBlockAsItem(world, pos, state, 0);
            world.setBlockToAir(pos);
        }
    }
    
    @Override
    public IBlockState getStateForPlacement(World world, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer, ItemStack stack)
    {
    	return this.getDefaultState().withProperty(ACTIVATED, false).withProperty(SILENT, stack.getMetadata() != 0);
    }
    
    @Override
    public void breakBlock(World world, BlockPos pos, IBlockState state)
    {
        if (this.isActiveted(state))
        {
            this.updateNeighbors(world, pos);
        }

        super.breakBlock(world, pos, state);
    }
    
    @Override
    public List<ItemStack> getDrops(IBlockAccess world, BlockPos pos, IBlockState state, int fortune)
    {
    	List<ItemStack> list = new ArrayList<ItemStack>();
    	boolean isSilent = (Boolean)state.getValue(SILENT).booleanValue();
    	
		list.add(isSilent ? new ItemStack(this, 1, 1) : new ItemStack(this, 1, 0));
    	
    	return list;
    }
    
    @Override
    public ItemStack getPickBlock(IBlockState state, RayTraceResult target, World world, BlockPos pos, EntityPlayer player)
    {
    	return (Boolean)state.getValue(SILENT).booleanValue() ? new ItemStack(this, 1, 1) : new ItemStack(this, 1, 0);
    }
    
    @Override
    public void randomTick(World world, BlockPos pos, IBlockState state, Random random)
    {
    }
    
    private boolean isSilent(IBlockState state)
    {
        return ((Boolean)state.getValue(SILENT)).booleanValue();
    }
    
    private boolean isActiveted(IBlockState state)
    {
        return ((Boolean)state.getValue(ACTIVATED)).booleanValue();
    }

    private IBlockState setRedstoneStrength(IBlockState state, int strength)
    {
        return state.withProperty(ACTIVATED, Boolean.valueOf(strength > 0));
    }
    
    protected int computeRedstoneStrength(World world, BlockPos pos)
    {
        AxisAlignedBB axisalignedbb = CHECKRANGE.offset(pos);
        List <? extends EntityPlayer > list;

        list = world.</*Entity*/EntityPlayer>getEntitiesWithinAABB(EntityPlayer.class, axisalignedbb);

        if (!list.isEmpty())
        {
            return 15;
        }

        return 0;
    }

    @Override
    public void updateTick(World world, BlockPos pos, IBlockState state, Random rand)
    {
        if (!world.isRemote)
        {
            if (this.isActiveted(state))
            {
                this.updateState(world, pos, state, 15);
            }
        }
    }
    
    @Override
    public void onEntityCollidedWithBlock(World worldIn, BlockPos pos, IBlockState state, Entity entityIn)
    {
    	if (!worldIn.isRemote)
        {
            if (!this.isActiveted(state))
            {
                this.updateState(worldIn, pos, state, 0);
            }
        }
    }
    
    protected void updateState(World world, BlockPos pos, IBlockState state, int oldRedstoneStrength)
    {
        int i = this.computeRedstoneStrength(world, pos);
        boolean wasActve = oldRedstoneStrength > 0;
        boolean isActiveNow = i > 0;

        if (oldRedstoneStrength != i)
        {
            state = this.setRedstoneStrength(state, i);
            world.setBlockState(pos, state, 2);
            this.updateNeighbors(world, pos);
            world.markBlockRangeForRenderUpdate(pos, pos);
        }

        if(!isSilent(state))
        {
	        if (!isActiveNow && wasActve)
	        {
	        	world.playSound((EntityPlayer)null, pos, SoundEvents.BLOCK_STONE_PRESSPLATE_CLICK_OFF, SoundCategory.BLOCKS, 0.3F, 0.5F);
	        }
	        else if (isActiveNow && !wasActve)
	        {
	        	world.playSound((EntityPlayer)null, pos, SoundEvents.BLOCK_STONE_PRESSPLATE_CLICK_ON, SoundCategory.BLOCKS, 0.3F, 0.6F);
	        }
        }

        if (isActiveNow)
        {
            world.scheduleUpdate(new BlockPos(pos), this, this.tickRate(world));
        }
    }
    
    private void updateNeighbors(World world, BlockPos pos)
    {
        world.notifyNeighborsOfStateChange(pos, this);
        world.notifyNeighborsOfStateChange(pos.down(), this);
    }

    public int getWeakPower(IBlockState blockState, IBlockAccess blockAccess, BlockPos pos, EnumFacing side)
    {
        return this.isActiveted(blockState) ? 15 : 0;
    }

    public int getStrongPower(IBlockState blockState, IBlockAccess blockAccess, BlockPos pos, EnumFacing side)
    {
        return side == EnumFacing.UP && this.isActiveted(blockState) ? 15 : 0;
    }

    public boolean canProvidePower(IBlockState state)
    {
        return true;
    }
    
    @Override
    public boolean canRegister()
    {
    	return RTConfig.getInstance().pressurePlate;
    }
}
