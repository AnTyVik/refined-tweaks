package sfaxx.refinedtweaks.inventory;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;
import sfaxx.refinedtweaks.blocks.tileentities.TileDisenchanter;
import sfaxx.refinedtweaks.inventory.component.OutputSlot;

public class ContainerDisenchanter extends Container
{
	private TileDisenchanter disenchanter;
	
	public ContainerDisenchanter(EntityPlayer player, TileDisenchanter disenchanter)
	{
		this.disenchanter = disenchanter;
		IItemHandler inv = this.disenchanter.getInventory();
		InventoryPlayer playerInv = player.inventory;
		
		for(int x = 0; x < 2; ++x)
		{
			addSlotToContainer(new SlotItemHandler(inv, x, 32 + x * 36, 37));
		}
		
		for(int x = 2; x < 4; ++x)
		{
			addSlotToContainer(new OutputSlot(inv, x, 104 + (x - 2) * 24, 37));
		}
		
		for(int y = 0; y < 3; ++y)
		{
			for(int x = 0; x < 9; ++x)
			{
				addSlotToContainer(new Slot(playerInv, x + y * 9 + 9, 8 + x * 18, 68 + y * 18));
			}
		}

		for(int x = 0; x < 9; ++x) 
		{
			addSlotToContainer(new Slot(playerInv, x, 8 + x * 18, 126));
		}
	}
	
	@Override
	public ItemStack transferStackInSlot(EntityPlayer player, int index)
	{
		ItemStack stack = null;
		Slot slot = inventorySlots.get(index);
		
		if(null != slot && slot.getHasStack())
		{
			ItemStack slotStack = slot.getStack();
			stack = slotStack.copy();
			
			if(index < 4)
			{
				if(!mergeItemStack(slotStack, 4, 40, true))
				{
					return null;
				}
			}
			else
			{
				Slot item = getSlot(0);
				Slot book = getSlot(1);
				
				if((item.isItemValid(stack) && !mergeItemStack(slotStack, 0, 1, true)) || (book.isItemValid(stack) && !mergeItemStack(slotStack, 1, 2, true)))
				{
					return null;
				}
			}
			
			if(0 == slotStack.stackSize)
			{
				slot.putStack(null);
			}
			else
			{
				slot.onSlotChanged();
			}
			
			if(stack.stackSize == slotStack.stackSize)
			{
				return null;
			}
			
			slot.onPickupFromSlot(player, slotStack);
		}
		
		return stack;
	}

	@Override
	public boolean canInteractWith(EntityPlayer player)
	{
		return player.getDistanceSq(disenchanter.getPos()) < 64;
	}
}
