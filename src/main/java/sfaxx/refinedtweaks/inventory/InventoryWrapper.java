package sfaxx.refinedtweaks.inventory;

import net.minecraft.item.ItemStack;
import net.minecraftforge.items.ItemStackHandler;

public class InventoryWrapper extends ItemStackHandler
{
	protected InventoryItemsHandler inner;
	protected int inputCount;

	public InventoryWrapper(InventoryItemsHandler inner, int size)
	{
		this(inner, size, size);
	}
	
	public InventoryWrapper(InventoryItemsHandler inner, int size, int inputCount)
	{
		super(size);
		this.inner = inner;
		this.inputCount = inputCount;
	}
	
	@Override
	public ItemStack insertItem(int slot, ItemStack stack, boolean simulate)
	{
		if(slot >= 0 && slot < inputCount && inner.isStackValidForSlot(stack, slot))
		{
			return super.insertItem(slot, stack, simulate);
		}
		
		return stack;
	}

	@Override
	public ItemStack extractItem(int slot, int amount, boolean simulate)
	{
		if(slot < inputCount)
		{
			return null;
		}
		
		return super.extractItem(slot, amount, simulate);
	}
}
