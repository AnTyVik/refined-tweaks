package sfaxx.refinedtweaks.inventory;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import sfaxx.refinedtweaks.blocks.tileentities.TileXPStorage;

public class ContainerXPStorage extends Container
{
	private TileXPStorage xpStorage;
	
	public ContainerXPStorage(TileXPStorage xpStorage)
	{
		this.xpStorage = xpStorage;
	}

	@Override
	public boolean canInteractWith(EntityPlayer player)
	{
		return player.getDistanceSq(xpStorage.getPos()) <= 64;
	}
}
