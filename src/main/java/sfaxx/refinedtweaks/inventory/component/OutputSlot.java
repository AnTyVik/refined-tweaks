package sfaxx.refinedtweaks.inventory.component;

import net.minecraft.item.ItemStack;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;

public class OutputSlot extends SlotItemHandler
{

	public OutputSlot(IItemHandler itemHandler, int index, int x, int y)
	{
		super(itemHandler, index, x, y);
	}

	@Override
	public boolean isItemValid(ItemStack stack)
	{
		return false;
	}	
}
