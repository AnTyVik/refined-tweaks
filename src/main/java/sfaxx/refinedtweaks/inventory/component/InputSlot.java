package sfaxx.refinedtweaks.inventory.component;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;

public class InputSlot extends SlotItemHandler
{
	public InputSlot(IItemHandler itemHandler, int index, int x, int y)
	{
		super(itemHandler, index, x, y);
	}

	@Override
	public ItemStack decrStackSize(int amount)
	{
		int index = this.getSlotIndex();
		ItemStack stack = this.getItemHandler().getStackInSlot(index);
		
		if(stack != null)
		{
			if(stack.stackSize > amount)
			{
				stack.stackSize -= amount;
				return stack.copy();
			}
		}
		
		return null;
	}
	
	@Override
	public boolean canTakeStack(EntityPlayer player)
	{
		return true;
	}
}
