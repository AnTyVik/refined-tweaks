package sfaxx.refinedtweaks.inventory;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;
import sfaxx.refinedtweaks.blocks.tileentities.TileVoider;
import sfaxx.refinedtweaks.inventory.component.OutputSlot;

public class ContainerVoider extends Container
{
	private TileVoider voider;
	
	public ContainerVoider(EntityPlayer player, TileVoider voider)
	{
		this.voider = voider;
		IItemHandler inv = this.voider.inv;
		IItemHandler invBuckets = this.voider.invBuckets;
		InventoryPlayer playerInv = player.inventory;
		
		for(int y = 0; y < 4; ++y)
		{
			for(int x = 0; x < 4; ++x)
			{
				addSlotToContainer(new SlotItemHandler(inv, x + y * 4, -6 + x * 18, 22 + y * 18));
			}
		}
		
		for(int x = 0; x < 2; ++x)
		{
			addSlotToContainer(new SlotItemHandler(invBuckets, x, 84 + x * 84, 28));
		}
		
		for(int x = 2; x < 4; ++x)
		{
			addSlotToContainer(new OutputSlot(invBuckets, x, 84 + (x - 2) * 84, 70));
		}
		
		for(int y = 0; y < 3; ++y)
		{
			for(int x = 0; x < 9; ++x)
			{
				addSlotToContainer(new Slot(playerInv, x + y * 9 + 9, 9 + x * 18, 107 + y * 18));
			}
		}

		for(int x = 0; x < 9; ++x) 
		{
			addSlotToContainer(new Slot(playerInv, x, 9 + x * 18, 165));
		}
	}
	
	@Override
	public ItemStack transferStackInSlot(EntityPlayer player, int index)
	{
		ItemStack stack = null;
		Slot slot = inventorySlots.get(index);
		
		if(null != slot && slot.getHasStack())
		{
			ItemStack slotStack = slot.getStack();
			stack = slotStack.copy();
			
			if(20 > index)
			{
				if(!mergeItemStack(slotStack, 20, 56, true))
				{
					return null;
				}		
			}
			else if(56 > index)
			{
				if(/*containerFull.isItemValid(stack) && */!mergeItemStack(slotStack, 16, 17, true))
				{
					if(/*containerEmpty.isItemValid(stack) && */!mergeItemStack(slotStack, 17, 18, true))
					{
						if(!mergeItemStack(slotStack, 0, 16, false))
						{
							return null;
						}
					}
				}
			}
			
			if(0 == slotStack.stackSize)
			{
				slot.putStack(null);
			}
			else
			{
				slot.onSlotChanged();
			}
			
			if(stack.stackSize == slotStack.stackSize)
			{
				return null;
			}
			
			slot.onPickupFromSlot(player, slotStack);
		}
		
		return stack;
	}

	@Override
	public boolean canInteractWith(EntityPlayer player)
	{
		return player.getDistanceSq(voider.getPos()) <= 64;
	}

}
