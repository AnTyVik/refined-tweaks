package sfaxx.refinedtweaks.inventory;

import net.minecraft.item.ItemStack;
import net.minecraftforge.items.ItemStackHandler;

public class InventoryItemsHandler extends ItemStackHandler
{	
	public InventoryItemsHandler(int size)
	{
		super(size);
	}
	
	@Override
	public ItemStack insertItem(int slot, ItemStack stack, boolean simulate)
	{
		if(this.isStackValidForSlot(stack, slot))
		{
			return super.insertItem(slot, stack, simulate);
		}
		
		return stack;
	}

	@Override
	public ItemStack extractItem(int slot, int amount, boolean simulate)
	{
		return super.extractItem(slot, amount, simulate);
	}
	
	protected boolean isStackValidForSlot(ItemStack stack, int slot)
	{
		return slot >= 0 && slot < this.getSlots();
	}
}
