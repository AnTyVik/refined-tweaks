package sfaxx.refinedtweaks.basics;

import net.minecraft.block.Block;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;

public class BasicItemBlock extends ItemBlock
{
	public BasicItemBlock(Block block)
	{
		this(block, false);
	}

    public BasicItemBlock(Block block, boolean hasSubtypes)
    {
        super(block);
        
        setRegistryName(block.getRegistryName());
        
        if (hasSubtypes) 
        {
            setMaxDamage(0);
            setHasSubtypes(true);
        }
    }
    
    @Override
    public int getMetadata(int damage)
    {
    	return damage;
    }
    
    @Override
    public String getUnlocalizedName(ItemStack stack)
    {
    	if(getHasSubtypes())
    	{
    		return getUnlocalizedName() + "." + stack.getItemDamage();
    	}
    	
    	return getUnlocalizedName();
    }
}
