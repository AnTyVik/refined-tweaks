package sfaxx.refinedtweaks.basics;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.ModelLoader;
import sfaxx.refinedtweaks.client.RTCreativeTab;
import sfaxx.refinedtweaks.references.Links;

public abstract class BasicBlock extends Block implements IRegisterable
{
    private String name;

    public BasicBlock(String name, Material material)
    {
        super(material);
        this.name = name;
        this.setCreativeTab(RTCreativeTab.getTab());
        this.setHardness(3F);
        this.setResistance(5.0f);
    }

    public BasicBlock(String name)
    {
        this(name, Material.IRON);
    }

    @Override
    public String getUnlocalizedName()
    {
        return String.format("%s:block.%s", Links.MODID, name);
    }
    
    protected BlockStateContainer.Builder createBlockStateBuilder() 
    {
        BlockStateContainer.Builder builder = new BlockStateContainer.Builder(this);

        return builder;
    }
    
    @Override
    protected BlockStateContainer createBlockState()
    {
    	return createBlockStateBuilder().build();
    }
    
    public ItemBlock createItemBlock()
    {
    	return new BasicItemBlock(this, false);
    }
    
    @Override
    public IBlockState getStateFromMeta(int meta)
    {
    	return getDefaultState();
    }
    
    @Override
    public int getMetaFromState(IBlockState state)
    {
    	return 0;
    }
    
    @Override
    public int damageDropped(IBlockState state)
    {
    	return getMetaFromState(state);
    }
    
    public Map<Integer, String> getVariants()
    {
    	Map<Integer, String> vars = new HashMap<Integer, String>();
    	vars.put(0, "inventory");
    	return vars;
    }

    @Override
    public void setResoureLocation()
    {
        Item blockItem = Item.getItemFromBlock(this);
        for(Entry<Integer, String> i : this.getVariants().entrySet())
        {
        	ModelLoader.setCustomModelResourceLocation(blockItem, i.getKey(), new ModelResourceLocation(new ResourceLocation(Links.MODID, name), i.getValue()/*"inventory"*/));
        }
    }
    
    @Override
    public abstract boolean canRegister();
}
