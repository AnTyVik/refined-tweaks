package sfaxx.refinedtweaks.basics;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.inventory.Container;

public abstract class BasicGuiContainer<T extends BasicTileEntity> extends GuiContainer
{
	protected T te;
	protected int guiWidth;
	protected int guiHeight;
	protected int left;
	protected int top;

	public BasicGuiContainer(Container container, T te, int guiWidth, int guiHeight)
	{
		super(container);
		this.te = te;
		this.guiWidth = guiWidth;
		this.guiHeight = guiHeight;
	}
	
	@Override
	public void initGui()
	{
		super.initGui();
		
		left = width / 2 - guiWidth / 2;
		top = height / 2 - guiHeight / 2;
		
		addButtons();
	}

	protected void addButtons()
	{
		
	}
}
