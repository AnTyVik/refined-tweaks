package sfaxx.refinedtweaks.basics;

import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.model.ModelLoader;
import sfaxx.refinedtweaks.client.RTCreativeTab;
import sfaxx.refinedtweaks.references.Links;

public abstract class BasicItem extends Item implements IRegisterable
{
	private String name;

	public BasicItem(String name, boolean hasSubitems)
	{
		this.name = name;
		if(hasSubitems)
		{
			setHasSubtypes(hasSubitems);
			setMaxDamage(0);
		}
		setCreativeTab(RTCreativeTab.getTab());
	}
	
	public BasicItem(String name)
	{
		this(name, false);
	}
	
	@Override
	public String getUnlocalizedName()
	{
		return String.format("%s:%s", Links.MODID, name);
	}
	
	@Override
	public String getUnlocalizedName(ItemStack stack)
	{
		return getHasSubtypes() ? String.format("%s.%d", getUnlocalizedName(), stack.getItemDamage()) : getUnlocalizedName();
	}
	
	@Override
	public void setResoureLocation()
	{
		ModelLoader.setCustomModelResourceLocation(this, 0, new ModelResourceLocation(getRegistryName(), "inventory"));
	}
}
