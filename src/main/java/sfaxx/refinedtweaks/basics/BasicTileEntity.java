package sfaxx.refinedtweaks.basics;

import net.minecraft.block.state.IBlockState;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;

public abstract class BasicTileEntity extends TileEntity
{
    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound)
    {
    	super.writeToNBT(compound);
    	writeCustomNBT(compound);
        return compound;
    }
    
    public abstract NBTTagCompound writeCustomNBT(NBTTagCompound compound);
    
    public NBTTagCompound writeSyncNBT(NBTTagCompound compound)
    {
    	return compound;
    }

    @Override
    public void readFromNBT(NBTTagCompound compound)
    {
        super.readFromNBT(compound);
        readCustomNBT(compound);
    }

    public abstract void readCustomNBT(NBTTagCompound compound);

    public void readSyncNBT(NBTTagCompound compound)
    {
    	
    }
    
    @Override
    public SPacketUpdateTileEntity getUpdatePacket()
    {
    	NBTTagCompound compound = writeToNBT(new NBTTagCompound());
    	compound = writeSyncNBT(compound);
    	
    	return new SPacketUpdateTileEntity(this.pos, 0, compound);
    }
    
    @Override
    public NBTTagCompound getUpdateTag()
    {
    	NBTTagCompound compound = writeToNBT(new NBTTagCompound());
    	compound = writeSyncNBT(compound);
    	
    	return compound;
    }
    
    @Override
    public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt)
    {
    	readCustomNBT(pkt.getNbtCompound());
    	readSyncNBT(pkt.getNbtCompound());
    }
    
    protected void updateContainingBlock(IBlockState newBlockState)
	{
        IBlockState state = worldObj.getBlockState(getPos());
        newBlockState = null == newBlockState ? state : newBlockState;
		worldObj.notifyBlockUpdate(pos, state,newBlockState, 3);
		worldObj.notifyNeighborsOfStateChange(pos, newBlockState.getBlock());
	}
}
