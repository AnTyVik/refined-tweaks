package sfaxx.refinedtweaks.util;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.VertexBuffer;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraftforge.fluids.Fluid;

public final class GuiUtils
{
	private GuiUtils() {}
	
	public static void drawTexture(float x, float y, float width, float height, double... uvs)
	{
		Tessellator tessellator = Tessellator.getInstance();
		VertexBuffer buffer = tessellator.getBuffer();
		buffer.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX);
		buffer.pos(x, y + height, 0).tex(uvs[0], uvs[3]).endVertex();
		buffer.pos(x + width, y + height, 0).tex(uvs[1], uvs[3]).endVertex();
		buffer.pos(x + width, y , 0).tex(uvs[1], uvs[2]).endVertex();
		buffer.pos(x, y , 0).tex(uvs[0], uvs[2]).endVertex();
		tessellator.draw();
	}
	
	public static void drawFluidInGui(Fluid fluid, float x, float y, float width, float height)
	{
		Minecraft.getMinecraft().getTextureManager().bindTexture(TextureMap.LOCATION_BLOCKS_TEXTURE);
		TextureAtlasSprite sprite = Minecraft.getMinecraft().getTextureMapBlocks().getAtlasSprite(fluid.getStill().toString());
		
		if(null == sprite)
		{
			return;
		}
		
		int iconWidth = sprite.getIconWidth();
		int iconHeight = sprite.getIconHeight();
		
		if(0 < iconWidth && 0 < iconHeight)
		{
			int drawWidth = (int) (width / iconWidth);
			int drawHeight = (int) (height / iconHeight);
			float extraWidth = width % iconWidth;
			float extraHeight = height % iconHeight;
			float extraIconWidth = extraWidth / (float) iconWidth;
			float extraIconHeight = extraHeight / (float) iconHeight;
			float dU = sprite.getMaxU() - sprite.getMinU();
			float dV = sprite.getMaxV() - sprite.getMinV();
			
			for(int h = 0; h < drawHeight; ++h)
			{
				for(int w = 0; w < drawWidth; ++w)
				{
					drawTexture(x + w * iconWidth, y + h * iconHeight, iconWidth, iconHeight, sprite.getMinU(), sprite.getMaxU(), sprite.getMinV(), sprite.getMaxV());
				}
				
				drawTexture(x + drawWidth * iconWidth, y + h * iconHeight, extraWidth, iconHeight, sprite.getMinU(), sprite.getMinU() + dU * extraIconWidth, sprite.getMinV(), sprite.getMaxV());
			}
			
			if(0 < extraHeight)
			{
				for(int w = 0; w < drawWidth; ++w)
				{
					drawTexture(x + w * iconWidth, y + drawHeight * iconHeight, iconWidth, extraHeight, sprite.getMinU(), sprite.getMaxU(), sprite.getMinV(), sprite.getMinV() + dV * extraIconHeight);
				}
				
				drawTexture(x + drawWidth * iconWidth, y + drawHeight * iconHeight, extraWidth, extraHeight, sprite.getMinU(), sprite.getMinU() + dU * extraIconWidth, sprite.getMinV(), sprite.getMinV() + dV * extraIconHeight);
			}
		}
	}
}
