package sfaxx.refinedtweaks.util;

import java.util.ArrayList;
import java.util.Random;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.VertexBuffer;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import sfaxx.refinedtweaks.RefinedTweaks;
import sfaxx.refinedtweaks.references.Links;
import vazkii.botania.common.item.ModItems;

public final class ItemStackUtil
{
	private ItemStackUtil() {}
	
	public static ArrayList<ItemStack> enchantable;
	
	public static void initEnchantableItems()
	{
		enchantable = new ArrayList<ItemStack>();
		
		enchantable.add(new ItemStack(Items.DIAMOND_SWORD));
		enchantable.add(new ItemStack(Items.DIAMOND_PICKAXE));
		enchantable.add(new ItemStack(Items.DIAMOND_AXE));
		enchantable.add(new ItemStack(Items.DIAMOND_SHOVEL));
		enchantable.add(new ItemStack(Items.DIAMOND_HOE));
		enchantable.add(new ItemStack(Items.BOW));
		enchantable.add(new ItemStack(Items.DIAMOND_HELMET));
		enchantable.add(new ItemStack(Items.DIAMOND_CHESTPLATE));
		enchantable.add(new ItemStack(Items.DIAMOND_LEGGINGS));
		enchantable.add(new ItemStack(Items.DIAMOND_BOOTS));
		enchantable.add(new ItemStack(Items.SHEARS));
		
		if(RefinedTweaks.botaniaLoaded)
		{
			enchantable.add(new ItemStack(ModItems.manasteelSword));
			enchantable.add(new ItemStack(ModItems.manasteelPick));
			enchantable.add(new ItemStack(ModItems.manasteelAxe));
			enchantable.add(new ItemStack(ModItems.manasteelShovel));
			enchantable.add(new ItemStack(ModItems.crystalBow));
			enchantable.add(new ItemStack(ModItems.manasteelHelm));
			enchantable.add(new ItemStack(ModItems.manasteelChest));
			enchantable.add(new ItemStack(ModItems.manasteelLegs));
			enchantable.add(new ItemStack(ModItems.manasteelBoots));
			enchantable.add(new ItemStack(ModItems.manasteelShears));
		}
		
		if(RefinedTweaks.eioLoaded)
		{
			enchantable.add(new ItemStack(Item.REGISTRY.getObject(new ResourceLocation(Links.EIO_MODID, "darkSteel_sword"))));
			enchantable.add(new ItemStack(Item.REGISTRY.getObject(new ResourceLocation(Links.EIO_MODID, "darkSteel_pickaxe"))));
			enchantable.add(new ItemStack(Item.REGISTRY.getObject(new ResourceLocation(Links.EIO_MODID, "darkSteel_axe"))));
			enchantable.add(new ItemStack(Item.REGISTRY.getObject(new ResourceLocation(Links.EIO_MODID, "darkSteelBow"))));
			enchantable.add(new ItemStack(Item.REGISTRY.getObject(new ResourceLocation(Links.EIO_MODID, "darkSteel_helmet"))));
			enchantable.add(new ItemStack(Item.REGISTRY.getObject(new ResourceLocation(Links.EIO_MODID, "darkSteel_chestplate"))));
			enchantable.add(new ItemStack(Item.REGISTRY.getObject(new ResourceLocation(Links.EIO_MODID, "darkSteel_leggings"))));
			enchantable.add(new ItemStack(Item.REGISTRY.getObject(new ResourceLocation(Links.EIO_MODID, "darkSteel_boots"))));
			enchantable.add(new ItemStack(Item.REGISTRY.getObject(new ResourceLocation(Links.EIO_MODID, "darkSteel_shears"))));
		}
		
		if(RefinedTweaks.bmLoaded)
		{
			enchantable.add(new ItemStack(Item.REGISTRY.getObject(new ResourceLocation(Links.BM_MODID.toLowerCase(), "ItemSentientSword"))));
			enchantable.add(new ItemStack(Item.REGISTRY.getObject(new ResourceLocation(Links.BM_MODID.toLowerCase(), "ItemSentientPickaxe"))));
			enchantable.add(new ItemStack(Item.REGISTRY.getObject(new ResourceLocation(Links.BM_MODID.toLowerCase(), "ItemSentientAxe"))));
			enchantable.add(new ItemStack(Item.REGISTRY.getObject(new ResourceLocation(Links.BM_MODID.toLowerCase(), "ItemSentientShovel"))));
			enchantable.add(new ItemStack(Item.REGISTRY.getObject(new ResourceLocation(Links.BM_MODID.toLowerCase(), "ItemSentientBow"))));
			enchantable.add(new ItemStack(Item.REGISTRY.getObject(new ResourceLocation(Links.BM_MODID.toLowerCase(), "ItemLivingArmourHelmet"))));
			enchantable.add(new ItemStack(Item.REGISTRY.getObject(new ResourceLocation(Links.BM_MODID.toLowerCase(), "ItemLivingArmourChest"))));
			enchantable.add(new ItemStack(Item.REGISTRY.getObject(new ResourceLocation(Links.BM_MODID.toLowerCase(), "ItemLivingArmourLegs"))));
			enchantable.add(new ItemStack(Item.REGISTRY.getObject(new ResourceLocation(Links.BM_MODID.toLowerCase(), "ItemLivingArmourBoots"))));
			enchantable.add(new ItemStack(Item.REGISTRY.getObject(new ResourceLocation(Links.BM_MODID.toLowerCase(), "ItemSentientArmourHelmet"))));
			enchantable.add(new ItemStack(Item.REGISTRY.getObject(new ResourceLocation(Links.BM_MODID.toLowerCase(), "ItemSentientArmourChest"))));
			enchantable.add(new ItemStack(Item.REGISTRY.getObject(new ResourceLocation(Links.BM_MODID.toLowerCase(), "ItemSentientArmourLegs"))));
			enchantable.add(new ItemStack(Item.REGISTRY.getObject(new ResourceLocation(Links.BM_MODID.toLowerCase(), "ItemSentientArmourBoots"))));
		}
	}
	
    public static ItemStack getRandomEnchantableItem()
    {
    	Random r = Minecraft.getMinecraft().theWorld.rand;
    	return enchantable.get(r.nextInt(enchantable.size())); 
    }
	
    public static void renderGhostItemStack(Minecraft mc, ItemStack stack, int x, int y)
    {
    	renderGhostItemStack(mc, stack, x, y, 0.8f);
    }
    
    public static void renderGhostItemStack(Minecraft mc, ItemStack stack, int x, int y, float r, float g, float b)
    {
    	renderGhostItemStack(mc, stack, x, y, r, g, b, 0.8f);
    }
    public static void renderGhostItemStack(Minecraft mc, ItemStack stack, int x, int y, float a)
    {
    	renderGhostItemStack(mc, stack, x, y, 139 / 255f, 139 / 255f, 139 / 255f, a);
    }
	
    public static void renderGhostItemStack(Minecraft mc, ItemStack stack, int x, int y, float r, float g, float b, float a)
    {
    	mc.getRenderItem().renderItemIntoGUI(stack, x, y);
    	
    	drawRect(mc, x, y, r, g, b, a);
    }

	private static void drawRect(Minecraft mc, int x, int y, float r, float g, float b, float a)
	{
		GlStateManager.color(r, g, b, a);
        GlStateManager.disableTexture2D();    
        GlStateManager.disableDepth();
        GlStateManager.enableBlend(); 
        Tessellator tessellator = Tessellator.getInstance();
        VertexBuffer tes = tessellator.getBuffer();
        tes.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION);
        tes.pos(x, y + 16, mc.getRenderItem().zLevel).endVertex();
        tes.pos(x + 16, y + 16, mc.getRenderItem().zLevel).endVertex();
        tes.pos(x + 16, y, mc.getRenderItem().zLevel).endVertex();
        tes.pos(x, y, mc.getRenderItem().zLevel).endVertex();
        tessellator.draw();
        GlStateManager.enableTexture2D();
	}
}
