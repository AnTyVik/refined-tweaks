package sfaxx.refinedtweaks.util;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraft.item.crafting.IRecipe;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.oredict.ShapedOreRecipe;

public class RecipeHelper
{
	public static void addBlockRecipe(ItemStack out, ItemStack in)
	{
		GameRegistry.addRecipe(out, "OOO", "OOO", "OOO", 'O', in);
	}

	public static void addBlockOreRecipe(ItemStack out, String oreIn)
	{
		GameRegistry.addRecipe(new ShapedOreRecipe(out, "OOO", "OOO", "OOO", 'O', oreIn));
	}
	
	public static void removeRecipesFor(ItemStack... results)
	{
		removeRecipesFor(Arrays.asList(results));
	}
	
	public static void removeRecipesFor(List<ItemStack> results)
	{
		List<IRecipe> recipes = CraftingManager.getInstance().getRecipeList();
        Iterator<IRecipe> it = recipes.iterator();

        while (it.hasNext())
        {
            ItemStack output = it.next().getRecipeOutput();
            for (ItemStack stack : results)
            {
                if(output != null && output.isItemEqual(stack))
                {
                    it.remove();
                }
            }
        }
	}
}