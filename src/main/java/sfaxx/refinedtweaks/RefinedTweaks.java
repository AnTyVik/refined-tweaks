package sfaxx.refinedtweaks;

import java.io.File;
import java.util.concurrent.TimeUnit;

import com.google.common.base.Stopwatch;

import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLInterModComms;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import sfaxx.refinedtweaks.handler.RTConfig;
import sfaxx.refinedtweaks.handler.RTGuiHandler;
import sfaxx.refinedtweaks.helper.XPHelper;
import sfaxx.refinedtweaks.proxy.CommonProxy;
import sfaxx.refinedtweaks.references.Links;
import sfaxx.refinedtweaks.util.ItemStackUtil;
import sfaxx.refinedtweaks.util.RTLogger;

@Mod(modid = Links.MODID, version = Links.VERSION, name = Links.NAME, dependencies = Links.DEPENDENCIES, guiFactory = Links.GUI_FACTORY)
public class RefinedTweaks
{
    @Mod.Instance(Links.MODID)
    public static RefinedTweaks instance;

    @SidedProxy(modId = Links.MODID, clientSide = Links.PROXY_CLIENT, serverSide = Links.PROXY_SERVER)
    public static CommonProxy proxy;

    
    public static boolean wailaLoaded = false;
    public static boolean topLoaded = false;
    public static boolean rsLoaded = false;
    public static boolean botaniaLoaded = false;
    public static boolean eioLoaded = false;
    public static boolean bmLoaded = false;
    public static boolean simplyjetpacksLoaded = false;
    public static boolean rftoolsLoaded = false;
    public static boolean rftoolsControlLoaded = false;
    public static boolean thermalExpansionLoaded = false;
    public static boolean mfrLoaded = false;

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent e)
    {
        final Stopwatch stopwatch = Stopwatch.createStarted();
        RTLogger.info("Pre Initialization started");
        
        RTConfig.initialize(new File(e.getModConfigurationDirectory(), Links.NAME + ".cfg"));
        
        wailaLoaded = Loader.isModLoaded(Links.RS_MODID);
        topLoaded = Loader.isModLoaded(Links.RS_MODID);
        rsLoaded = Loader.isModLoaded(Links.RS_MODID);
        botaniaLoaded = Loader.isModLoaded(Links.BOTANIA_MODID);
        eioLoaded = Loader.isModLoaded(Links.EIO_MODID);
        bmLoaded = Loader.isModLoaded(Links.BM_MODID);
        simplyjetpacksLoaded = Loader.isModLoaded(Links.SIMPLY_JETPACKS_MODID);
        rftoolsLoaded = Loader.isModLoaded(Links.SIMPLY_JETPACKS_MODID);
        rftoolsControlLoaded = Loader.isModLoaded(Links.SIMPLY_JETPACKS_MODID);
        thermalExpansionLoaded = Loader.isModLoaded(Links.THERMAL_EXPANSION_MODID);
        mfrLoaded = Loader.isModLoaded(Links.MFR_MODID);
        
        XPHelper.initXPMap();
        proxy.initNetwork();

        proxy.registerContent();
        proxy.initResourceLocations();
        
        proxy.preInitCompat();

        RTLogger.info("Pre Initialization ended (" + stopwatch.elapsed(TimeUnit.MILLISECONDS) + " ms)");
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent e)
    {
        final Stopwatch stopwatch = Stopwatch.createStarted();
        RTLogger.info("Initialization started");

        proxy.initRecipes();
        
        NetworkRegistry.INSTANCE.registerGuiHandler(RefinedTweaks.instance, new RTGuiHandler());
        
        FMLInterModComms.sendMessage("Waila", "register", "sfaxx.refinedtweaks.compat.waila.RTWailaDataProvider.callbackRegister");
        proxy.initCompat();
        

        RTLogger.info("Initialization ended (" + stopwatch.elapsed(TimeUnit.MILLISECONDS) + " ms)");
    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent e)
    {
        final Stopwatch stopwatch = Stopwatch.createStarted();
        RTLogger.info("Post Initialization started");

        ItemStackUtil.initEnchantableItems();
        proxy.postInitCompat();
        
        RTLogger.info("Post Initialization ended (" + stopwatch.elapsed(TimeUnit.MILLISECONDS) + " ms)");
    }
}
