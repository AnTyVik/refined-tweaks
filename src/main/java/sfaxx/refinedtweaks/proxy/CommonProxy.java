package sfaxx.refinedtweaks.proxy;

import sfaxx.refinedtweaks.compat.RTCompat;
import sfaxx.refinedtweaks.init.RTBlocks;
import sfaxx.refinedtweaks.init.RTItems;
import sfaxx.refinedtweaks.init.RTRecipes;
import sfaxx.refinedtweaks.init.RTTileEntities;
import sfaxx.refinedtweaks.network.RTPacketHandler;

public class CommonProxy
{
    public void registerContent()
    {
        RTBlocks.registerBlocks();
        RTItems.registerItems();
        RTTileEntities.registerTileEntities();
    }

    public void initResourceLocations()
    {

    }

    public void initNetwork()
    {
		RTPacketHandler.registerMesages();
    }

	public void initRecipes()
	{        
        RTRecipes.init();
	}
	
	public void preInitCompat()
	{
		RTCompat.registerCompats();
		RTCompat.preInitCompatModules();
	}
	
	public void initCompat()
	{
//		RTWailaDataProvider.init();
		RTCompat.initCompatModules();
	}

	public void postInitCompat()
	{
		RTCompat.postInitCompatModules();
	}
}
