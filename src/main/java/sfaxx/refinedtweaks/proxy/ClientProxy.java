package sfaxx.refinedtweaks.proxy;

import net.minecraftforge.client.model.obj.OBJLoader;
import sfaxx.refinedtweaks.init.RTBlocks;
import sfaxx.refinedtweaks.init.RTItems;
import sfaxx.refinedtweaks.references.Links;

public class ClientProxy extends CommonProxy
{
    @Override
    public void initResourceLocations()
    {
        OBJLoader.INSTANCE.addDomain(Links.MODID);
        RTBlocks.setResourceLocations();
        RTItems.setResourceLocations();
    }
}
