package sfaxx.refinedtweaks.network;

import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.Side;
import sfaxx.refinedtweaks.references.Links;

public class RTPacketHandler
{
	public static final SimpleNetworkWrapper INSTANCE = NetworkRegistry.INSTANCE.newSimpleChannel(String.format("%schnl", Links.MODID));
	
	private static int 	id;
	
	public static int nextId()
	{
		return id++;
	}
	
	public static void registerMesages()
	{
		INSTANCE.registerMessage(MessageXpStorage.class, MessageXpStorage.class, nextId(), Side.SERVER);
		INSTANCE.registerMessage(MessageVoider.class, MessageVoider.class, nextId(), Side.SERVER);
	}
}
