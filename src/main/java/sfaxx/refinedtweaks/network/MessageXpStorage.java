package sfaxx.refinedtweaks.network;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import sfaxx.refinedtweaks.blocks.tileentities.TileXPStorage;
import sfaxx.refinedtweaks.blocks.tileentities.TileXPStorage.Mode;

public class MessageXpStorage implements IMessage, IMessageHandler<MessageXpStorage, IMessage>
{
	int x;
	int y;
	int z;
	int transfer;
	
	public MessageXpStorage()
	{
	}
	
	public MessageXpStorage(TileXPStorage xpStorage, int transfer)
	{
		BlockPos pos = xpStorage.getPos();
		this.x = pos.getX();
		this.y = pos.getY();
		this.z = pos.getZ();
		this.transfer = transfer;
	}

	@Override
	public void fromBytes(ByteBuf buf)
	{
		x = buf.readInt();
		y = buf.readInt();
		z = buf.readInt();
		transfer = buf.readInt(); 
	}

	@Override
	public void toBytes(ByteBuf buf)
	{
		buf.writeInt(x);
		buf.writeInt(y);
		buf.writeInt(z);
		buf.writeInt(transfer);
	}

	@Override
	public IMessage onMessage(MessageXpStorage msg, MessageContext ctx)
	{
		EntityPlayer player = ctx.getServerHandler().playerEntity;
		if(null != player.worldObj)
		{
			TileEntity te = player.worldObj.getTileEntity(new BlockPos(msg.x, msg.y, msg.z));
			
			if(null != te && te instanceof TileXPStorage)
			{
				if(msg.transfer > 0)
				{
					( (TileXPStorage) te).getXpFromPlayer(player, Mode.fromInt(msg.transfer));
				}
				else if(msg.transfer < 0)
				{
					( (TileXPStorage) te).transferXpToPlayer(player, Mode.fromInt(msg.transfer));
				}
			}
		}
		
		return null;
	}

}
