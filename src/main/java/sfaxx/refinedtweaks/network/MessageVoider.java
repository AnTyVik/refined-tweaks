package sfaxx.refinedtweaks.network;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import sfaxx.refinedtweaks.blocks.tileentities.TileVoider;

public class MessageVoider implements IMessage, IMessageHandler<MessageVoider, IMessage>
{
	int x;
	int y;
	int z;
	byte action;
	boolean actionParam;
	
	public MessageVoider()
	{
		
	}
	
	public MessageVoider(TileVoider voider, int action, boolean actionParam)
	{
		BlockPos pos = voider.getPos();
		x = pos.getX();
		y = pos.getY();
		z = pos.getZ();
		this.action = (byte) action;
		this.actionParam = actionParam;
	}
	
	@Override
	public void fromBytes(ByteBuf buf)
	{
		x = buf.readInt();
		y = buf.readInt();
		z = buf.readInt();
		
		action = buf.readByte();
		actionParam = buf.readBoolean();
	}

	@Override
	public void toBytes(ByteBuf buf)
	{
		buf.writeInt(x);
		buf.writeInt(y);
		buf.writeInt(z);
		
		buf.writeByte(action);
		buf.writeBoolean(actionParam);
	}

	@Override
	public IMessage onMessage(MessageVoider msg, MessageContext ctx)
	{
		EntityPlayer player = ctx.getServerHandler().playerEntity;
		if(null != player.worldObj)
		{
			TileEntity te = player.worldObj.getTileEntity(new BlockPos(msg.x, msg.y, msg.z));
			
			if(null != te && te instanceof TileVoider)
			{
				switch(msg.action)
				{
					case 0:
					{
						( (TileVoider) te).clearInventory();
						break;
					}
					case 1:
					{
						( (TileVoider) te).clearTank();
						break;
					}
					case 2:
					{
						( (TileVoider) te).setCanConnectForItems(msg.actionParam);
						break;
					}
					case 3:
					{
						( (TileVoider) te).setCanConnectForFluids(msg.actionParam);
						break;
					}
				}
			}
		}
		
		return null;
	}

}
