package sfaxx.refinedtweaks.compat;

import java.util.LinkedList;
import java.util.ListIterator;

import sfaxx.refinedtweaks.RefinedTweaks;
import sfaxx.refinedtweaks.compat.refinedStorage.RTRefinedStorageCompat;
import sfaxx.refinedtweaks.compat.rftools.RTRFToolsCompat;
import sfaxx.refinedtweaks.compat.simplyjetpacks.RTSimplyJetpacksCompat;
import sfaxx.refinedtweaks.compat.theoneprobe.RTTheOneProbeProvider;
import sfaxx.refinedtweaks.compat.thermalexpansion.RTThermalExpansionCompat;

public abstract class RTCompat
{
	private static LinkedList<RTCompat> compatModules;

	protected RTCompat()
	{

	}

	public static void registerCompats()
	{
		compatModules = new LinkedList<RTCompat>();

//		if (RefinedTweaks.wailaLoaded)
//		{
//			compatModules.add(new RTWailaDataProvider());
//		}
		if (RefinedTweaks.topLoaded)
		{
			compatModules.add(new RTTheOneProbeProvider());
		}
		
		compatModules.add(new RTRefinedStorageCompat());
		compatModules.add(new RTSimplyJetpacksCompat());
		compatModules.add(new RTRFToolsCompat());
		compatModules.add(new RTThermalExpansionCompat());
	}

	public static void preInitCompatModules()
	{
		ListIterator<RTCompat> i = compatModules.listIterator();

		while (i.hasNext())
		{
			i.next().preInit();
		}
	}

	public static void initCompatModules()
	{
		ListIterator<RTCompat> i = compatModules.listIterator();

		while (i.hasNext())
		{
			i.next().init();
		}
	}

	public static void postInitCompatModules()
	{
		ListIterator<RTCompat> i = compatModules.listIterator();

		while (i.hasNext())
		{
			i.next().postInit();
		}
	}

	public abstract void preInit();

	public abstract void init();

	public abstract void postInit();
}
