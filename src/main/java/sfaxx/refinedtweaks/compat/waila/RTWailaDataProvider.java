package sfaxx.refinedtweaks.compat.waila;

import java.util.List;

import mcp.mobius.waila.api.IWailaConfigHandler;
import mcp.mobius.waila.api.IWailaDataAccessor;
import mcp.mobius.waila.api.IWailaDataProvider;
import mcp.mobius.waila.api.IWailaRegistrar;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.event.FMLInterModComms;
import sfaxx.refinedtweaks.blocks.BlockPlayerPressurePlate;
import sfaxx.refinedtweaks.blocks.tileentities.TileXPStorage;
import sfaxx.refinedtweaks.compat.RTCompat;
import sfaxx.refinedtweaks.init.RTBlocks;

//TODO: Add localization
public class RTWailaDataProvider extends RTCompat implements IWailaDataProvider
{
	public RTWailaDataProvider()
	{
		
	}
	
	@Override
	public void preInit()
	{
		
	}
	
	@Override
	public void init()
	{
		FMLInterModComms.sendMessage("Waila", "register", "sfaxx.refinedtweaks.compat.waila.RTWailaDataProvider.callbackRegister");
	}
	
	@Override
	public void postInit()
	{
			
	}
	
    public static void callbackRegister(IWailaRegistrar registrar)
    {
        RTWailaDataProvider dataProvider = new RTWailaDataProvider();
        registrar.registerBodyProvider(dataProvider, TileXPStorage.class);
    }

    @Override
    public ItemStack getWailaStack(IWailaDataAccessor accessor, IWailaConfigHandler config)
    {
        return null;
    }

    @Override
    public List<String> getWailaHead(ItemStack itemStack, List<String> currenttip, IWailaDataAccessor accessor, IWailaConfigHandler config)
    {
        return currenttip;
    }

    @Override
    public List<String> getWailaBody(ItemStack itemStack, List<String> currenttip, IWailaDataAccessor accessor, IWailaConfigHandler config)
    {
        Block b = accessor.getBlock();
        TileEntity te = accessor.getTileEntity();

        if(te instanceof TileXPStorage)
        {
        	TileXPStorage xpStorage = (TileXPStorage) te;
            int xp = xpStorage.getXp();
            int level = xpStorage.getLevel();
            int progress = (int) (xpStorage.getBarXP() * 100);

            if(accessor.getPlayer().isSneaking())
            {
                currenttip.add(String.format("%s : %d", "Total XP", xp));
            }

            currenttip.add(String.format("%s : %d", "Level", level));
            currenttip.add(String.format("%s : %d%%", "Progress to next level", progress));
        }
        if(RTBlocks.PLAYER_PLATE.block == b)
        {
        	if(( (Boolean) accessor.getBlockState().getValue(BlockPlayerPressurePlate.SILENT)).booleanValue())
			{
        		currenttip.add("Silent");
			}
        }

        return currenttip;
    }

    @Override
    public List<String> getWailaTail(ItemStack itemStack, List<String> currenttip, IWailaDataAccessor accessor, IWailaConfigHandler config)
    {
        return currenttip;
    }

    @Override
    public NBTTagCompound getNBTData(EntityPlayerMP player, TileEntity te, NBTTagCompound tag, World world, BlockPos pos)
    {
    	return tag;
    }
}
