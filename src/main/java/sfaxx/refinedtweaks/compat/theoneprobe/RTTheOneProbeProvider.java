package sfaxx.refinedtweaks.compat.theoneprobe;

import javax.annotation.Nullable;

import com.google.common.base.Function;

import mcjty.theoneprobe.api.IProbeHitData;
import mcjty.theoneprobe.api.IProbeInfo;
import mcjty.theoneprobe.api.IProbeInfoProvider;
import mcjty.theoneprobe.api.ITheOneProbe;
import mcjty.theoneprobe.api.NumberFormat;
import mcjty.theoneprobe.api.ProbeMode;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.event.FMLInterModComms;
import sfaxx.refinedtweaks.blocks.BlockPlayerPressurePlate;
import sfaxx.refinedtweaks.blocks.tileentities.TileXPStorage;
import sfaxx.refinedtweaks.compat.RTCompat;
import sfaxx.refinedtweaks.init.RTBlocks;
import sfaxx.refinedtweaks.references.Links;

//TODO: Add localization
public class RTTheOneProbeProvider extends RTCompat implements Function<ITheOneProbe, Void>
{
	public RTTheOneProbeProvider()
	{
		
	}
	
	@Override
	public void preInit()
	{
        FMLInterModComms.sendFunctionMessage("theoneprobe", "getTheOneProbe", RTTheOneProbeProvider.class.getName());
	}
	
	@Override
	public void init()
	{
			
	}
	
	@Override
	public void postInit()
	{
			
	}
	
	@Nullable
	@Override
	public Void apply(@Nullable ITheOneProbe probe)
	{
		probe.registerProvider(new XpInfoProvider());
		
		return null;
	}

	public static class XpInfoProvider implements IProbeInfoProvider
	{

		@Override
		public String getID()
		{
			return String.format("%s:%S", Links.MODID, "XpInfo");
		}

		@SuppressWarnings("unused")
		@Override
		public void addProbeInfo(ProbeMode mode, IProbeInfo probeInfo, EntityPlayer player, World world, IBlockState blockState, IProbeHitData data)
		{
			int w = probeInfo.defaultItemStyle().getWidth() / 2;
			int h = probeInfo.defaultItemStyle().getHeight() / 2;
			
			if(RTBlocks.XP_STORAGE.block == blockState.getBlock())
			{
				TileEntity te = world.getTileEntity(data.getPos());
				
				int total = 0;
				int level = 0;
				int progress = 0;
				
				if(te instanceof TileXPStorage)
				{
					TileXPStorage xpStorage = (TileXPStorage) te;
					total = xpStorage.getXp();
					level = ( (TileXPStorage) te).getLevel();
					progress = (int) (xpStorage.getBarXP() * 100f);
				}
				
				if(player.isSneaking())
				{
					probeInfo.text(String.format("%s: %d", "Total XP", total));
				}
				probeInfo.text(String.format("%s: %d", "Level", level));
				probeInfo.progress(progress, 100, probeInfo.defaultProgressStyle().suffix(progress + "%").filledColor(0xff60dd00).alternateFilledColor(0xff00dd00).borderColor(0xff006000).numberFormat(NumberFormat.NONE));
			}
			else if(RTBlocks.PLAYER_PLATE.block == blockState.getBlock())
			{
				if(( (Boolean) blockState.getValue(BlockPlayerPressurePlate.SILENT)).booleanValue())
				{
					probeInfo.text("Silent");
				}
				if(false)
				{
					probeInfo.horizontal().item(new ItemStack(Items.REDSTONE), probeInfo.defaultItemStyle().width(w).height(h)).text("Normal");
					probeInfo.horizontal().item(new ItemStack(Blocks.REDSTONE_TORCH), probeInfo.defaultItemStyle().width(w).height(h)).text("Inverted");
				}
			}
		}
		
	}
}
