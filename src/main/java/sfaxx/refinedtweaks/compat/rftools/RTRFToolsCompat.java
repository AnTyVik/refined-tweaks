package sfaxx.refinedtweaks.compat.rftools;

import com.raoulvdberge.refinedstorage.RSItems;

import net.minecraft.block.Block;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.oredict.OreDictionary;
import net.minecraftforge.oredict.ShapedOreRecipe;
import sfaxx.refinedtweaks.RefinedTweaks;
import sfaxx.refinedtweaks.compat.RTCompat;
import sfaxx.refinedtweaks.handler.RTConfig;
import sfaxx.refinedtweaks.util.RTLogger;
import sfaxx.refinedtweaks.util.RecipeHelper;
import vazkii.botania.api.BotaniaAPI;

public class RTRFToolsCompat extends RTCompat
{

	public RTRFToolsCompat()
	{
		
	}
	
	@Override
	public void preInit()
	{
			
	}

	public void init()
	{
		
	}
	
	@Override
	public void postInit()
	{
		if(RefinedTweaks.rftoolsLoaded && RTConfig.getInstance().changeRFToolsRecipes)
		{
			RTLogger.info("Found RF Tools. Tweaky tweaky.");
			overrideRFToolsRecipes();
		}
		if(RefinedTweaks.rftoolsControlLoaded && RTConfig.getInstance().changeRFToolsControlRecipes)
		{
			RTLogger.info("Found RF Tools Control. Making tweaks in the name of control!");
		}	
	}

	private static void overrideRFToolsRecipes()
	{
		Block dimShardOre = Block.REGISTRY.getObject(new ResourceLocation("rftools", "dimensional_shard_ore"));
		Block machineFrame = Block.REGISTRY.getObject(new ResourceLocation("rftools", "machine_frame"));
		Block machineBase = Block.REGISTRY.getObject(new ResourceLocation("rftools", "machine_base"));
		
		ItemStack core = RefinedTweaks.rsLoaded ? new ItemStack(RSItems.PROCESSOR, 1, 1) : new ItemStack(Items.GOLD_NUGGET);
		String component = "plateSteel";
		
		if(null != dimShardOre)
		{
			OreDictionary.registerOre("oreDimensionalShard", dimShardOre);
			BotaniaAPI.addOreWeight("oreDimensionalShard", 512);
		}
		
		if(null != machineFrame)
		{
			RecipeHelper.removeRecipesFor(new ItemStack(machineFrame));
			GameRegistry.addRecipe(new ShapedOreRecipe(machineFrame, "ccc", "coc", "ccc", 'c', component, 'o', core));
		}
		
		if(null != machineBase)
		{
			RecipeHelper.removeRecipesFor(new ItemStack(machineBase));
			GameRegistry.addShapelessRecipe(new ItemStack(machineBase, 2), machineFrame);
			GameRegistry.addShapedRecipe(new ItemStack(machineFrame), "b", "b", 'b', new ItemStack(machineBase));
		}
	}
}
