package sfaxx.refinedtweaks.compat.thermalexpansion;

import com.raoulvdberge.refinedstorage.RSItems;
import com.raoulvdberge.refinedstorage.item.ItemProcessor;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fml.common.event.FMLInterModComms;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.oredict.ShapedOreRecipe;
import sfaxx.refinedtweaks.RefinedTweaks;
import sfaxx.refinedtweaks.compat.RTCompat;
import sfaxx.refinedtweaks.references.Links;
import sfaxx.refinedtweaks.util.RecipeHelper;

public class RTThermalExpansionCompat extends RTCompat
{

	public RTThermalExpansionCompat()
	{
		
	}

	@Override
	public void preInit()
	{
		
	}

	@Override
	public void init()
	{
		
	}

	@Override
	public void postInit()
	{
		//Block frame = Block.REGISTRY.getObject(new ResourceLocation(Links.THERMAL_EXPANSION_MODID, "frame"));
		Item frame = Item.REGISTRY.getObject(new ResourceLocation(Links.THERMAL_EXPANSION_MODID, "frame"));
		ItemStack frameMachine = new ItemStack(frame, 1, 0);
		ItemStack frameDevice = new ItemStack(frame, 1, 64);
		ItemStack frameEnergy = new ItemStack(frame, 1, 128);
		ItemStack pulverizer = new ItemStack(Block.REGISTRY.getObject(new ResourceLocation(Links.THERMAL_EXPANSION_MODID, "machine")), 1, 1);
		String oreHardenedGlass = "blockGlassHardened";
		String plate = "plateAluminum";
		
		RecipeHelper.removeRecipesFor(frameMachine, frameDevice, frameEnergy, pulverizer);
		
		GameRegistry.addRecipe(new ShapedOreRecipe(frameMachine, "pgp", "gcg", "pgp", 'p', plate, 'g', oreHardenedGlass, 'c', new ItemStack(RSItems.PROCESSOR, 1, ItemProcessor.TYPE_ADVANCED)));
		GameRegistry.addRecipe(new ShapedOreRecipe(frameDevice, "pgp", "gcg", "pgp", 'p', plate, 'g', oreHardenedGlass, 'c', "gearCopper"));
		GameRegistry.addRecipe(new ShapedOreRecipe(frameEnergy, "pgp", "gcg", "pgp", 'p', plate, 'g', oreHardenedGlass, 'c', "gearLead"));
		GameRegistry.addRecipe(new ShapedOreRecipe(pulverizer, " p ", "dfd", "gcg", 'p', Blocks.PISTON, 'd', Items.DIAMOND, 'f', frameMachine, 'g', "gearCopper", 'c', new ItemStack(RSItems.PROCESSOR, 1, ItemProcessor.TYPE_ADVANCED)));
		
		if(RefinedTweaks.mfrLoaded)
		{
			Block rubberSapling = Block.REGISTRY.getObject(new ResourceLocation(Links.MFR_MODID, "rubberwood_sapling"));
			NBTTagCompound recipe = new NBTTagCompound();
			recipe.setInteger("energy", 4000);
			recipe.setTag("input", new NBTTagCompound());
			ItemStack sapStack = new ItemStack(Blocks.SAPLING, 1, 1);
			sapStack.writeToNBT(recipe.getCompoundTag("input"));
			ItemStack rubStack = new ItemStack(rubberSapling, 1, 0);
			recipe.setTag("output", new NBTTagCompound());
			rubStack.writeToNBT(recipe.getCompoundTag("output"));
			recipe.setTag("fluid", new NBTTagCompound());
			FluidStack fs = new FluidStack(FluidRegistry.getFluid("resin"), 1000);
			fs.writeToNBT(recipe.getCompoundTag("fluid"));
			recipe.setBoolean("reversible", false);
			FMLInterModComms.sendMessage(Links.THERMAL_EXPANSION_MODID, "addtransposerfillrecipe", recipe);
		}
	}

}
