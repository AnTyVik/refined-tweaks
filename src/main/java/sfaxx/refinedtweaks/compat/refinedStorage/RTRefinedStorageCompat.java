package sfaxx.refinedtweaks.compat.refinedStorage;

import java.util.ArrayList;

import com.raoulvdberge.refinedstorage.RSBlocks;
import com.raoulvdberge.refinedstorage.RSItems;

import blusunrize.immersiveengineering.api.crafting.CrusherRecipe;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.oredict.ShapedOreRecipe;
import sfaxx.refinedtweaks.RefinedTweaks;
import sfaxx.refinedtweaks.compat.RTCompat;
import sfaxx.refinedtweaks.handler.RTConfig;
import sfaxx.refinedtweaks.util.RecipeHelper;

public class RTRefinedStorageCompat extends RTCompat
{
	private static ArrayList<ItemStack> results = new ArrayList<ItemStack>();

	public RTRefinedStorageCompat()
	{

	}

	@Override
	public void preInit()
	{

	}

	@Override
	public void init()
	{

	}

	public void postInit()
	{
		if (RefinedTweaks.rsLoaded && RTConfig.getInstance().changeRSRecipes)
		{
			results.add(new ItemStack(RSItems.QUARTZ_ENRICHED_IRON));
			results.add(new ItemStack(RSBlocks.MACHINE_CASING));
			results.add(new ItemStack(RSBlocks.SOLDERER));
			results.add(new ItemStack(RSBlocks.WIRELESS_TRANSMITTER));
			results.add(new ItemStack(RSBlocks.GRID, 1, 0));
			results.add(new ItemStack(RSItems.WIRELESS_GRID, 1, 0));
			results.add(new ItemStack(RSItems.WIRELESS_CRAFTING_MONITOR, 1, 0));
			results.add(new ItemStack(RSBlocks.CRAFTER));
			results.add(new ItemStack(RSBlocks.PROCESSING_PATTERN_ENCODER));
			results.add(new ItemStack(RSBlocks.DESTRUCTOR));
			results.add(new ItemStack(RSBlocks.CONSTRUCTOR));
			results.add(new ItemStack(RSBlocks.DETECTOR));
			results.add(new ItemStack(RSItems.STORAGE_PART, 1, 0));
			results.add(new ItemStack(RSItems.STORAGE_PART, 1, 1));
			results.add(new ItemStack(RSItems.STORAGE_PART, 1, 2));
			results.add(new ItemStack(RSItems.STORAGE_PART, 1, 3));
			results.add(new ItemStack(RSItems.GRID_FILTER));
			results.add(new ItemStack(RSItems.NETWORK_CARD));
			results.add(new ItemStack(RSBlocks.DISK_MANIPULATOR));
			results.add(new ItemStack(RSItems.WRENCH));
			results.add(new ItemStack(RSBlocks.CONTROLLER));
			results.add(new ItemStack(RSBlocks.CABLE));
			results.add(new ItemStack(RSBlocks.EXTERNAL_STORAGE));
			results.add(new ItemStack(RSItems.FLUID_STORAGE_PART, 1, 0));
			results.add(new ItemStack(RSItems.FLUID_STORAGE_PART, 1, 1));
			results.add(new ItemStack(RSItems.FLUID_STORAGE_PART, 1, 2));
			results.add(new ItemStack(RSItems.FLUID_STORAGE_PART, 1, 3));
			results.add(new ItemStack(RSItems.STORAGE_HOUSING));
			results.add(new ItemStack(RSItems.STORAGE_DISK, 1, 0));
			results.add(new ItemStack(RSItems.STORAGE_DISK, 1, 1));
			results.add(new ItemStack(RSItems.STORAGE_DISK, 1, 2));
			results.add(new ItemStack(RSItems.STORAGE_DISK, 1, 3));
			results.add(new ItemStack(RSItems.FLUID_STORAGE_DISK, 1, 0));
			results.add(new ItemStack(RSItems.FLUID_STORAGE_DISK, 1, 1));
			results.add(new ItemStack(RSItems.FLUID_STORAGE_DISK, 1, 2));
			results.add(new ItemStack(RSItems.FLUID_STORAGE_DISK, 1, 3));
			results.add(new ItemStack(RSItems.PATTERN));
			results.add(new ItemStack(RSItems.UPGRADE, 1, 0));
			results.add(new ItemStack(RSBlocks.CRAFTING_MONITOR));

			removeRecipes();
			addRecipes();
		}
	}

	private static void removeRecipes()
	{
		RecipeHelper.removeRecipesFor(results);

	}

	private static void addRecipes()
	{
		CrusherRecipe.addRecipe(new ItemStack(RSItems.SILICON), Blocks.SAND, 3200);

		// String newComponent = "ingotSteel";
		String newComponent = "plateAluminum";
		GameRegistry.addRecipe(
				new ShapedOreRecipe(new ItemStack(RSBlocks.MACHINE_CASING), "sss", "s s", "sss", 's', newComponent));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(RSBlocks.SOLDERER), "sps", "s s", "sps", 's',
				newComponent, 'p', Blocks.STICKY_PISTON));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(RSBlocks.WIRELESS_TRANSMITTER), "sps", "scs", "sas",
				's', newComponent, 'p', Items.ENDER_PEARL, 'c', RSBlocks.MACHINE_CASING, 'a',
				new ItemStack(RSItems.PROCESSOR, 1, 5)));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(RSBlocks.GRID, 1, 0), "scs", "imi", "sds", 's',
				newComponent, 'c', new ItemStack(RSItems.CORE, 1, 0), 'i', new ItemStack(RSItems.PROCESSOR, 1, 4), 'm',
				RSBlocks.MACHINE_CASING, 'd', new ItemStack(RSItems.CORE, 1, 1)));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(RSItems.WIRELESS_GRID, 1, 0), "sps", "sgs", "sas", 's',
				newComponent, 'p', Items.ENDER_PEARL, 'g', new ItemStack(RSBlocks.GRID, 1, 0), 'a',
				new ItemStack(RSItems.PROCESSOR, 1, 5)));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(RSItems.WIRELESS_CRAFTING_MONITOR, 1, 0), "sps", "sgs",
				"sas", 's', newComponent, 'p', Items.ENDER_PEARL, 'g', RSBlocks.CRAFTING_MONITOR, 'a',
				new ItemStack(RSItems.PROCESSOR, 1, 5)));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(RSBlocks.CRAFTER), "scs", "ama", "sds", 's',
				newComponent, 'c', new ItemStack(RSItems.CORE, 1, 0), 'm', RSBlocks.MACHINE_CASING, 'a',
				new ItemStack(RSItems.PROCESSOR, 1, 5), 'd', new ItemStack(RSItems.CORE, 1, 1)));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(RSBlocks.PROCESSING_PATTERN_ENCODER), "sts", "pcp",
				"sfs", 's', newComponent, 'c', RSBlocks.MACHINE_CASING, 't', Blocks.CRAFTING_TABLE, 'p',
				new ItemStack(RSItems.PATTERN), 'f', Blocks.FURNACE));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(RSBlocks.DESTRUCTOR), "scs", "rbr", "sis", 's',
				newComponent, 'c', new ItemStack(RSItems.CORE, 1, 1), 'r', "dustRedstone", 'b', RSBlocks.CABLE, 'i',
				new ItemStack(RSItems.PROCESSOR, 1, 4)));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(RSBlocks.CONSTRUCTOR), "scs", "rbr", "sis", 's',
				newComponent, 'c', new ItemStack(RSItems.CORE, 1, 0), 'r', "dustRedstone", 'b', RSBlocks.CABLE, 'i',
				new ItemStack(RSItems.PROCESSOR, 1, 4)));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(RSBlocks.DETECTOR), "ECE", "RMR", "EPE", 'E',
				newComponent, 'R', "dustRedstone", 'C', Items.COMPARATOR, 'M', RSBlocks.MACHINE_CASING, 'P',
				new ItemStack(RSItems.PROCESSOR, 1, 4)));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(RSItems.STORAGE_PART, 1, 0), "sis", "grg", "sgs", 'i',
				newComponent, 's', RSItems.SILICON, 'g', "blockGlass", 'r', "dustRedstone"));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(RSItems.STORAGE_PART, 1, 1), "psp", "grg", "pgp", 's',
				newComponent, 'p', new ItemStack(RSItems.PROCESSOR, 1, 3), 'g',
				new ItemStack(RSItems.STORAGE_PART, 1, 0), 'r', "dustRedstone"));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(RSItems.STORAGE_PART, 1, 2), "psp", "grg", "pgp", 's',
				newComponent, 'p', new ItemStack(RSItems.PROCESSOR, 1, 4), 'g',
				new ItemStack(RSItems.STORAGE_PART, 1, 1), 'r', "dustRedstone"));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(RSItems.STORAGE_PART, 1, 3), "psp", "grg", "pgp", 's',
				newComponent, 'p', new ItemStack(RSItems.PROCESSOR, 1, 5), 'g',
				new ItemStack(RSItems.STORAGE_PART, 1, 2), 'r', "dustRedstone"));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(RSItems.GRID_FILTER), "sps", "php", "sps", 's',
				newComponent, 'p', "paper", 'h', Blocks.HOPPER));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(RSItems.NETWORK_CARD), "sss", "pap", "sss", 's',
				newComponent, 'p', "paper", 'a', new ItemStack(RSItems.PROCESSOR, 1, 5)));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(RSBlocks.DISK_MANIPULATOR), "shs", "cmd", "shs", 's',
				newComponent, 'c', new ItemStack(RSItems.CORE, 1, 0), 'h', RSItems.STORAGE_HOUSING, 'm',
				RSBlocks.MACHINE_CASING, 'd', new ItemStack(RSItems.CORE, 1, 1)));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(RSItems.WRENCH), "sbs", "sss", " s ", 's',
				newComponent, 'b', new ItemStack(RSItems.PROCESSOR, 1, 3)));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(RSBlocks.CONTROLLER, 1, 0), "sas", "imi", "sis", 's',
				newComponent, 'a', new ItemStack(RSItems.PROCESSOR, 1, 5), 'i', RSItems.SILICON, 'm',
				RSBlocks.MACHINE_CASING));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(RSBlocks.CABLE, 12, 0), "sss", "grg", "sss", 's',
				newComponent, 'g', "blockGlass", 'r', "dustRedstone"));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(RSBlocks.EXTERNAL_STORAGE, 1, 0), "csd", "tbt", "sis",
				's', newComponent, 'c', new ItemStack(RSItems.CORE, 1, 0), 'i', new ItemStack(RSItems.PROCESSOR, 1, 4),
				't', "chest", 'd', new ItemStack(RSItems.CORE, 1, 1), 'b', RSBlocks.CABLE));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(RSItems.FLUID_STORAGE_PART, 1, 0), "sis", "grg", "sgs",
				'i', newComponent, 's', RSItems.SILICON, 'g', "blockGlass", 'r', Items.BUCKET));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(RSItems.FLUID_STORAGE_PART, 1, 1), "psp", "grg", "pgp",
				's', newComponent, 'p', new ItemStack(RSItems.PROCESSOR, 1, 3), 'g',
				new ItemStack(RSItems.FLUID_STORAGE_PART, 1, 0), 'r', Items.BUCKET));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(RSItems.FLUID_STORAGE_PART, 1, 2), "psp", "grg", "pgp",
				's', newComponent, 'p', new ItemStack(RSItems.PROCESSOR, 1, 4), 'g',
				new ItemStack(RSItems.FLUID_STORAGE_PART, 1, 1), 'r', Items.BUCKET));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(RSItems.FLUID_STORAGE_PART, 1, 3), "psp", "grg", "pgp",
				's', newComponent, 'p', new ItemStack(RSItems.PROCESSOR, 1, 5), 'g',
				new ItemStack(RSItems.FLUID_STORAGE_PART, 1, 2), 'r', Items.BUCKET));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(RSItems.STORAGE_HOUSING), "grg", "r r", "sss", 's',
				newComponent, 'g', "blockGlass", 'r', "dustRedstone"));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(RSItems.STORAGE_DISK, 1, 0), "grg", "rpr", "sss", 's',
				newComponent, 'g', "blockGlass", 'r', "dustRedstone", 'p', new ItemStack(RSItems.STORAGE_PART, 1, 0)));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(RSItems.STORAGE_DISK, 1, 1), "grg", "rpr", "sss", 's',
				newComponent, 'g', "blockGlass", 'r', "dustRedstone", 'p', new ItemStack(RSItems.STORAGE_PART, 1, 1)));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(RSItems.STORAGE_DISK, 1, 2), "grg", "rpr", "sss", 's',
				newComponent, 'g', "blockGlass", 'r', "dustRedstone", 'p', new ItemStack(RSItems.STORAGE_PART, 1, 2)));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(RSItems.STORAGE_DISK, 1, 3), "grg", "rpr", "sss", 's',
				newComponent, 'g', "blockGlass", 'r', "dustRedstone", 'p', new ItemStack(RSItems.STORAGE_PART, 1, 3)));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(RSItems.FLUID_STORAGE_DISK, 1, 0), "grg", "rpr", "sss",
				's', newComponent, 'g', "blockGlass", 'r', "dustRedstone", 'p',
				new ItemStack(RSItems.FLUID_STORAGE_PART, 1, 0)));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(RSItems.FLUID_STORAGE_DISK, 1, 1), "grg", "rpr", "sss",
				's', newComponent, 'g', "blockGlass", 'r', "dustRedstone", 'p',
				new ItemStack(RSItems.FLUID_STORAGE_PART, 1, 1)));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(RSItems.FLUID_STORAGE_DISK, 1, 2), "grg", "rpr", "sss",
				's', newComponent, 'g', "blockGlass", 'r', "dustRedstone", 'p',
				new ItemStack(RSItems.FLUID_STORAGE_PART, 1, 2)));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(RSItems.FLUID_STORAGE_DISK, 1, 3), "grg", "rpr", "sss",
				's', newComponent, 'g', "blockGlass", 'r', "dustRedstone", 'p',
				new ItemStack(RSItems.FLUID_STORAGE_PART, 1, 3)));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(RSItems.PATTERN), "grg", "rgr", "sss", 's',
				newComponent, 'g', "blockGlass", 'r', "dustRedstone"));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(RSItems.UPGRADE), "sgs", "sps", "sgs", 's',
				newComponent, 'g', "blockGlass", 'p', new ItemStack(RSItems.PROCESSOR, 1, 4)));
		GameRegistry.addRecipe(
				new ShapedOreRecipe(new ItemStack(RSBlocks.CRAFTING_MONITOR), "sgs", "gcg", "sps", 's', newComponent,
						'g', "blockGlass", 'p', new ItemStack(RSItems.PROCESSOR, 1, 4), 'c', RSBlocks.MACHINE_CASING));
	}
}
