package sfaxx.refinedtweaks.compat.simplyjetpacks;

import java.util.Iterator;
import java.util.List;

import blusunrize.immersiveengineering.api.crafting.ArcFurnaceRecipe;
import blusunrize.immersiveengineering.api.crafting.IngredientStack;
import blusunrize.immersiveengineering.common.IEContent;
import blusunrize.immersiveengineering.common.IERecipes;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.oredict.ShapedOreRecipe;
import net.minecraftforge.oredict.ShapelessOreRecipe;
import sfaxx.refinedtweaks.RefinedTweaks;
import sfaxx.refinedtweaks.compat.RTCompat;
import sfaxx.refinedtweaks.handler.RTConfig;
import sfaxx.refinedtweaks.init.RTBlocks;
import sfaxx.refinedtweaks.init.RTItems;
import sfaxx.refinedtweaks.references.Links;
import sfaxx.refinedtweaks.references.Resources;
import sfaxx.refinedtweaks.util.RTLogger;
import vazkii.botania.common.block.ModBlocks;

public class RTSimplyJetpacksCompat extends RTCompat
{
	public RTSimplyJetpacksCompat()
	{
		
	}
	
	@Override
	public void preInit()
	{
			
	}
	
	@Override
	public void init()
	{
			
	}

	public void postInit()
	{
        if(RefinedTweaks.simplyjetpacksLoaded && RTConfig.getInstance().changeSJRecipes)
        {
        	Item jp = Item.REGISTRY.getObject(new ResourceLocation(Links.SIMPLY_JETPACKS_MODID, "itemJetpack"));
        	
        	if(null == jp)
        	{
        		RTLogger.error("NO JETPACK!");
        	}
        	else
        	{
        		RTLogger.info("Found Simply Jetpacks 2. Making simple tweaks too.");
        		initSJRecipes(jp);
        	}
        }
	}

	private static void initSJRecipes(Item jetpack)
	{
		Item metaItem = Item.REGISTRY.getObject(new ResourceLocation(Links.SIMPLY_JETPACKS_MODID, "metaItem"));
		removeJetpacksRecipe(new ItemStack(jetpack, 1, 11), new ItemStack(jetpack, 1, 12), new ItemStack(jetpack, 1, 13), new ItemStack(metaItem, 1, 4), new ItemStack(metaItem, 1, 5), new ItemStack(metaItem, 1, 6));
		
		GameRegistry.addSmelting(RTBlocks.ORE_TIN.block, new ItemStack(RTItems.INGOT.item, 1, Resources.TIN.getMeta()), 0.7f);
		GameRegistry.addSmelting(new ItemStack(RTItems.DUST.item, 1, Resources.TIN.getMeta()), new ItemStack(RTItems.INGOT.item, 1, Resources.TIN.getMeta()), 0.7f);
		GameRegistry.addSmelting(new ItemStack(RTItems.DUST.item, 1, Resources.INVAR.getMeta()), new ItemStack(RTItems.INGOT.item, 1, Resources.INVAR.getMeta()), 0.7f);
		IERecipes.addOreDictAlloyingRecipe("ingotInvar", 3, "Nickel", 100, 512, new IngredientStack("ingotIron", 2));
		IERecipes.addOreDictAlloyingRecipe(new ItemStack(RTItems.INGOT.item, 4, Resources.LAPISENDULI.getMeta()), "Silver", 400, 1024, new IngredientStack("ingotTin", 2), new ItemStack(Items.DYE, 4, 4), new ItemStack(Items.ENDER_PEARL, 4), new ItemStack(Items.BLAZE_POWDER));
		IERecipes.addOreDictAlloyingRecipe(new ItemStack(RTItems.INGOT.item, 4, Resources.LAPISENDULI.getMeta()), "Silver", 400, 1024, new IngredientStack("dustTin", 2), new ItemStack(Items.DYE, 4, 4), new ItemStack(Items.ENDER_PEARL, 4), new ItemStack(Items.BLAZE_POWDER));
		ArcFurnaceRecipe.addRecipe(new ItemStack(RTItems.BUCKET_REDSTONE.item, 1), (Object) new IngredientStack(new ItemStack(Items.BUCKET)), null, 150, 512, new IngredientStack("dustRedstone", 10)).setSpecialRecipeType("Alloying");
		GameRegistry.addRecipe(new ShapelessOreRecipe(new ItemStack(RTItems.DUST.item, 3, Resources.INVAR.getMeta()), "dustNickel", "dustIron", "dustIron"));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(RTItems.COIL.item, 1), " CR", "CEC", "RC ", 'C', "ingotCopper", 'R', "dustRedstone", 'E', "ingotElectrum"));
		
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(metaItem, 1, 4), "ICI", "IBI", "IRI", 'I', "ingotInvar", 'C', new ItemStack(RTItems.COIL.item), 'B', new ItemStack(ModBlocks.blazeBlock), 'R', new ItemStack(Blocks.REDSTONE_BLOCK)));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(metaItem, 1, 5), "ICI", "IBI", "IRI", 'I', "ingotElectrum", 'C', new ItemStack(RTItems.COIL.item), 'B', new ItemStack(ModBlocks.blazeBlock), 'R', new ItemStack(RTItems.BUCKET_REDSTONE.item)));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(metaItem, 1, 6), "ICI", "IBI", "IRI", 'I', new ItemStack(RTItems.INGOT.item, 4, Resources.LAPISENDULI.getMeta()), 'C', new ItemStack(RTItems.COIL.item), 'B', new ItemStack(ModBlocks.blazeBlock), 'R', new ItemStack(RTItems.BUCKET_REDSTONE.item)));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(jetpack, 1, 11), "ICI", "IJI", "T T", 'I', "ingotInvar", 'C', new ItemStack(IEContent.blockMetalDevice0, 1, 0), 'J', new ItemStack(metaItem, 1, 7), 'T', new ItemStack(metaItem, 1, 4)));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(jetpack, 1, 12), "ICI", "IJI", "T T", 'I', "ingotElectrum", 'C', new ItemStack(IEContent.blockMetalDevice0, 1, 0), 'J', new ItemStack(jetpack, 1, 11), 'T', new ItemStack(metaItem, 1, 5)));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(jetpack, 1, 13), "ICI", "IJI", "T T", 'I', new ItemStack(RTItems.INGOT.item, 4, Resources.LAPISENDULI.getMeta()), 'C', new ItemStack(IEContent.blockMetalDevice0, 1, 0), 'J', new ItemStack(jetpack, 1, 12), 'T', new ItemStack(metaItem, 1, 6)));
	}
	
	private static void removeJetpacksRecipe(ItemStack... toRemove)
	{
		List<IRecipe> recipes = CraftingManager.getInstance().getRecipeList();
        Iterator<IRecipe> it = recipes.iterator();

        while (it.hasNext())
        {
            ItemStack output = it.next().getRecipeOutput();
            for (ItemStack stack : toRemove)
            {
                if(output != null && output.isItemEqual(stack))
                {
                	RTLogger.info(String.format("Removing recipe for %s", stack.getDisplayName()));
                    it.remove();
                }
            }
        }
	}
}
