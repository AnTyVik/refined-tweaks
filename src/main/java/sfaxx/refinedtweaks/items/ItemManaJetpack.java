package sfaxx.refinedtweaks.items;

import net.minecraft.inventory.EntityEquipmentSlot;
import vazkii.botania.common.item.equipment.armor.manasteel.ItemManasteelArmor;

public class ItemManaJetpack extends ItemManasteelArmor
{
	public ItemManaJetpack(String name)
	{
		super(EntityEquipmentSlot.CHEST, name);
	}

}
