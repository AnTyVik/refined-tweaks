package sfaxx.refinedtweaks.items;

import sfaxx.refinedtweaks.RefinedTweaks;
import sfaxx.refinedtweaks.basics.BasicItem;
import sfaxx.refinedtweaks.handler.RTConfig;

public class ItemBucketRedstone extends BasicItem
{
	public ItemBucketRedstone(String name, boolean hasSubitems)
	{
		super(name, hasSubitems);
	}

	public ItemBucketRedstone(String name)
	{
		super(name);
	}

	@Override
	public boolean canRegister()
	{
		return RefinedTweaks.simplyjetpacksLoaded && RTConfig.getInstance().changeSJRecipes;
	}

}
