package sfaxx.refinedtweaks.items;

import sfaxx.refinedtweaks.RefinedTweaks;
import sfaxx.refinedtweaks.basics.BasicItem;
import sfaxx.refinedtweaks.handler.RTConfig;

public class ItemConductiveCoil extends BasicItem
{
	public ItemConductiveCoil(String name)
	{
		super(name);
	}

	@Override
	public boolean canRegister()
	{
		return RefinedTweaks.simplyjetpacksLoaded && RTConfig.getInstance().changeSJRecipes;
	}

}
