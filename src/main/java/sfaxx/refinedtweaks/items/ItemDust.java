package sfaxx.refinedtweaks.items;

import java.util.List;

import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.ModelLoader;
import sfaxx.refinedtweaks.RefinedTweaks;
import sfaxx.refinedtweaks.basics.BasicItem;
import sfaxx.refinedtweaks.handler.RTConfig;
import sfaxx.refinedtweaks.references.Resources;

public class ItemDust extends BasicItem
{

	public ItemDust(String name)
	{
		super(name, true);
	}

	@Override
	public void getSubItems(Item itemIn, CreativeTabs tab, List<ItemStack> subItems)
	{
		for(Resources r : Resources.values())
		{
			if(r.hasDust())
			{
				subItems.add(new ItemStack(itemIn, 1, r.getMeta()));
			}
		}
	}
	
	@Override
	public void setResoureLocation()
	{
		for(Resources r : Resources.values())
		{
			ModelLoader.setCustomModelResourceLocation(this, r.getMeta(), new ModelResourceLocation(new ResourceLocation(getRegistryName().getResourceDomain(), getRegistryName().getResourcePath() + r.getName()), "inventory"));
		}
	}

	@Override
	public boolean canRegister()
	{
		return RefinedTweaks.simplyjetpacksLoaded && RTConfig.getInstance().changeSJRecipes;
	}

}
