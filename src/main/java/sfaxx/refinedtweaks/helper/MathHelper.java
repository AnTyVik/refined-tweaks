package sfaxx.refinedtweaks.helper;

public final class MathHelper
{
	private MathHelper()
	{
		
	}
	
	public static int lerp(int i, int min, int max)
	{
		return i < min ? min : (i > max ? max : i);
	}
	
	public static double lerp(double i, double min, double max)
	{
		return i < min ? min : (i > max ? max : i);
	}
	
	public static boolean isInRange(double i, double min, double max)
	{
		return i >= min && i <= max;
	}
}
