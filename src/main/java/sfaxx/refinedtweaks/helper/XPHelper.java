package sfaxx.refinedtweaks.helper;

import net.minecraft.entity.player.EntityPlayer;

/**
 * Inspired by crazypants.enderio.xp.XpUtil.java [@author CrazyPants]
 *
 * @see <a href="https://github.com/SleepyTrousers/EnderIO/blob/1.10/src/main/java/crazypants/enderio/xp/XpUtil.java">EnderIO XpUtil</a>
 * @see <a href="https://github.com/OpenMods/OpenModsLib/blob/master/src/main/java/openmods/utils/EnchantmentUtils.java">OpenMods EnchantmentUtils</a>
 */

public final class XPHelper
{
    private XPHelper()
    {

    }

    private static final int[] xpMap = new int[256];

    public static void initXPMap()
    {
        int xp = 0;
        for(int level = 0; level < xpMap.length; ++level)
        {
            xp += getBarCapacity(level);

            if(xp < 0)
            {
                xpMap[level] = Integer.MAX_VALUE;
            }
            else
            {
                xpMap[level] = xp;
            }
        }
    }

    private static int getXpFromLevel(int l)
    {
        /*if(l < xpMap.length)
        {
            return getExperienceForLevel(l);
        }*/

        if(l <= 0)
        {
            return 0;
        }

        int i = xpMap.length;
        int xp = xpMap[i - 1];
        while(i <= l) ;
        {
            xp += getBarCapacity(l);
            ++i;
        }

        return xp;
    }

    public static int getExperienceForLevel(int level)
    {
        if(level > 0 && level < xpMap.length)
        {
            return xpMap[level - 1];
        }
        if(level >= 21863)
        {
            return Integer.MAX_VALUE;
        }

        return getXpFromLevel(level);
    }

    public static int getLevelForExperience(int xp)
    {
        int i = xp > xpMap[192] ? 192 : (xp > xpMap[128] ? 128 : (xp > xpMap[64] ? 64 : 0));

        for(; i < xpMap.length; ++i)
        {
            if(xp < xpMap[i])
            {
                return i/* - 1*/;
            }
        }

        i = xpMap.length;
        while(getExperienceForLevel(i) <= xp)
        {
            i++;
        }
        return i - 1;
    }

    public static int getBarCapacity(int level)
    {
        return level >= 30 ? 112 + (level - 30) * 9 : (level >= 15 ? 37 + (level - 15) * 5 : 7 + level * 2);
    }

    public static int getPlayerXP(EntityPlayer player)
    {
        return (int) (getExperienceForLevel(player.experienceLevel) + (player.experience * player.xpBarCap()));
    }

    public static void addPlayerXP(EntityPlayer player, int amount)
    {
        int experience = Math.max(0, getPlayerXP(player) + amount);
        player.experienceTotal = experience;
        player.experienceLevel = getLevelForExperience(experience);
        int expForLevel = getExperienceForLevel(player.experienceLevel);
        player.experience = (float) (experience - expForLevel) / (float) player.xpBarCap();
    }
}
