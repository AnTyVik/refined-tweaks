package sfaxx.refinedtweaks.init;

import net.minecraft.block.Block;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.registry.GameRegistry;
import sfaxx.refinedtweaks.basics.BasicBlock;
import sfaxx.refinedtweaks.basics.IRegisterable;
import sfaxx.refinedtweaks.blocks.BlockDisenchanter;
import sfaxx.refinedtweaks.blocks.BlockOfCharcoal;
import sfaxx.refinedtweaks.blocks.BlockOreTin;
import sfaxx.refinedtweaks.blocks.BlockPlayerPressurePlate;
import sfaxx.refinedtweaks.blocks.BlockVoider;
import sfaxx.refinedtweaks.blocks.BlockXPStorage;
import sfaxx.refinedtweaks.references.Links;
import sfaxx.refinedtweaks.references.Names;
import sfaxx.refinedtweaks.util.RTLogger;

public enum RTBlocks
{
	ORE_TIN(new BlockOreTin(Names.Blocks.ORE_TIN), Names.Blocks.ORE_TIN),
    XP_STORAGE(new BlockXPStorage(Names.Blocks.XP_STORAGE), Names.Blocks.XP_STORAGE),
    CHARCOAL_BLOCK(new BlockOfCharcoal(Names.Blocks.CHARCOAL_BLOCK), Names.Blocks.CHARCOAL_BLOCK),
    PLAYER_PLATE(new BlockPlayerPressurePlate(Names.Blocks.PLAYER_PLATE), Names.Blocks.PLAYER_PLATE),
    DISENCHANTER(new BlockDisenchanter(Names.Blocks.DISENCHANTER), Names.Blocks.DISENCHANTER),
    VOIDER(new BlockVoider(Names.Blocks.VOIDER), Names.Blocks.VOIDER)
    ;

    public final Block block;
    private String name;

    RTBlocks(Block block, String name)
    {
        this.block = block;
        this.name = name;
    }

    public static void registerBlocks()
    {
        for(RTBlocks b : RTBlocks.values())
        {
        	if(b.block instanceof IRegisterable && ( (IRegisterable) b.block).canRegister())
        	{
        		b.register();
        	}
        }
    }

    public static void setResourceLocations()
    {
        for(RTBlocks b : RTBlocks.values())
        {
        	if(b.block instanceof IRegisterable && ( (IRegisterable) b.block).canRegister())
        	{
            	((IRegisterable) b.block).setResoureLocation();
        	}
        }
    }

    public void register()
    {
        try
        {
            ResourceLocation location = new ResourceLocation(Links.MODID, name);
            block.setRegistryName(location);
            GameRegistry.<Block>register(block);
            GameRegistry.register(((BasicBlock) block).createItemBlock());
        }
        catch(Exception e)
        {
            RTLogger.error(String.format("Unable to register '%s' block", name));
            e.printStackTrace();
        }
    }
}
