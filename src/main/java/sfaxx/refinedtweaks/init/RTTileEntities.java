package sfaxx.refinedtweaks.init;

import net.minecraftforge.fml.common.registry.GameRegistry;
import sfaxx.refinedtweaks.blocks.tileentities.TileDisenchanter;
import sfaxx.refinedtweaks.blocks.tileentities.TileVoider;
import sfaxx.refinedtweaks.blocks.tileentities.TileXPStorage;
import sfaxx.refinedtweaks.references.Names;

public class RTTileEntities
{
    public static void registerTileEntities()
    {
        GameRegistry.registerTileEntity(TileXPStorage.class, Names.TileEntities.XP_STORAGE);
        GameRegistry.registerTileEntity(TileDisenchanter.class, Names.TileEntities.DISENCHANTER);
        GameRegistry.registerTileEntity(TileVoider.class, Names.TileEntities.VOIDER);
    }
}
