package sfaxx.refinedtweaks.init;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.oredict.OreDictionary;
import sfaxx.refinedtweaks.RefinedTweaks;
import sfaxx.refinedtweaks.basics.IRegisterable;
import sfaxx.refinedtweaks.handler.RTConfig;
import sfaxx.refinedtweaks.items.ItemBucketRedstone;
import sfaxx.refinedtweaks.items.ItemConductiveCoil;
import sfaxx.refinedtweaks.items.ItemDust;
import sfaxx.refinedtweaks.items.ItemIngot;
import sfaxx.refinedtweaks.references.Links;
import sfaxx.refinedtweaks.references.Names;
import sfaxx.refinedtweaks.references.Resources;
import sfaxx.refinedtweaks.util.RTLogger;

public enum RTItems
{
//	MANA_JETPACK(RefinedTweaks.botaniaLoaded ? new ItemManaJetpack(Names.Items.MANA_JETPACK) : null,Names.Items.MANA_JETPACK)
	INGOT(new ItemIngot(Names.Items.INGOT), Names.Items.INGOT),
	DUST(new ItemDust(Names.Items.DUST), Names.Items.DUST),
	BUCKET_REDSTONE(new ItemBucketRedstone(Names.Items.BUCKET_REDSTONE), Names.Items.BUCKET_REDSTONE),
	COIL(new ItemConductiveCoil(Names.Items.COIL), Names.Items.COIL)
	;
	
	public final Item item;
    private String name;
	
	private RTItems(Item item, String name)
	{
		this.item = item;
		this.name = name;
	}

	public static void registerItems()
    {
        for(RTItems i : RTItems.values())
        {
        	if(i.item instanceof IRegisterable && ( (IRegisterable) i.item).canRegister())
        	{
        		i.register();
        	}
        }
        
        initOredict();
    }
	
    public static void setResourceLocations()
    {
        for(RTItems i : RTItems.values())
        {
        	if(i.item instanceof IRegisterable && ( (IRegisterable) i.item).canRegister())
        	{
            	((IRegisterable) i.item).setResoureLocation();
        	}
        }
    }

    public void register()
    {
        try
        {
            ResourceLocation location = new ResourceLocation(Links.MODID, name);
            item.setRegistryName(location);
            GameRegistry.<Item>register(item);
        }
        catch(Exception e)
        {
            RTLogger.error(String.format("Unable to register '%s' item", name));
            e.printStackTrace();
        }
    }
    
    private static void initOredict()
	{
    	
		if(RefinedTweaks.simplyjetpacksLoaded && RTConfig.getInstance().changeSJRecipes)
		{
			for(Resources r : Resources.values())
			{
				if(r.hasOreId())
				{
					OreDictionary.registerOre(r.getOreName("ingot"), new ItemStack(INGOT.item, 1, r.getMeta()));
					OreDictionary.registerOre(r.getOreName("dust"), new ItemStack(DUST.item, 1, r.getMeta()));
				}
			}
			
			OreDictionary.registerOre("oreTin", new ItemStack(RTBlocks.ORE_TIN.block));
		}
	} 
}
