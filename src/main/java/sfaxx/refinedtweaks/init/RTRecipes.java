package sfaxx.refinedtweaks.init;

import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.registry.GameRegistry;
import sfaxx.refinedtweaks.RefinedTweaks;
import sfaxx.refinedtweaks.basics.IRegisterable;
import sfaxx.refinedtweaks.util.RecipeHelper;
import vazkii.botania.common.block.ModBlocks;
import vazkii.botania.common.item.ModItems;

public class RTRecipes
{
	private RTRecipes()
	{
		
	}
	
	//TODO: Add missing recipes
	public static void init()
	{		
		if(( (IRegisterable) RTBlocks.XP_STORAGE.block).canRegister())
		{
			if(RefinedTweaks.botaniaLoaded)
				GameRegistry.addShapedRecipe(new ItemStack(RTBlocks.XP_STORAGE.block), "   ", "GMG", "MBM", 'M', new ItemStack(ModItems.manaResource, 1, 0), 'G', new ItemStack(ModBlocks.manaGlass, 1, 0), 'B', new ItemStack(ModItems.vial, 1, 0));
			else
				GameRegistry.addShapedRecipe(new ItemStack(RTBlocks.XP_STORAGE.block), "   ", "GIG", "IBI", 'I', new ItemStack(Items.GOLD_INGOT), 'G', new ItemStack(Blocks.GLASS, 1, 0), 'B', new ItemStack(Items.GLASS_BOTTLE, 1, 0));
		}
		
		if(( (IRegisterable) RTBlocks.XP_STORAGE.block).canRegister())
		{
			RecipeHelper.addBlockOreRecipe(new ItemStack(RTBlocks.CHARCOAL_BLOCK.block), "charcoal");
		}
	}
}
