package sfaxx.refinedtweaks.references;

public final class Names
{
    public final class Blocks
    {
    	public static final String ORE_TIN = "oreTin";
        public static final String XP_STORAGE = "xpStorage";
        public static final String CHARCOAL_BLOCK = "charcoal";
        public static final String PLAYER_PLATE = "playerPressurePlate";
        public static final String DISENCHANTER = "disenchanter";
        public static final String VOIDER = "voider";
    }
    
    public final class Items
    {
    	public static final String MANA_JETPACK = Links.MODID + ":" + "manaJetpack";
    	public static final String INGOT = "ingot";
    	public static final String DUST = "dust";
    	public static final String BUCKET_REDSTONE = "bucketOfRedstone";
    	public static final String COIL = "coil";
    }

    public final class TileEntities
    {
        public static final String XP_STORAGE = "xpStorage";
        public static final String DISENCHANTER = "disenchanter";
        public static final String VOIDER = "voider";
    }
    
    public final class Textures
    {
    	public final class GUI
        {
    		public static final String GUI_LOCATION = Links.MODID + ":textures/gui/";
    		public static final String XP_STORAGE = GUI_LOCATION + "xpStorage.png";
    		public static final String DISENCHANTER = GUI_LOCATION + "disenchanter.png";
    		public static final String VOIDER = GUI_LOCATION + "voider.png";
        }
    }
}
