package sfaxx.refinedtweaks.references;

import javax.annotation.Nullable;

public enum Resources
{
	//0-15: basic res.; 16-31: basic alloys; 32+: adv. alloys
	TIN("Tin", 0),
	INVAR("Invar", 16),
	LAPISENDULI("LapisEnduli", 32, false, true, false);
	
	private String name; 
	private int meta;
	private boolean hasOreId;
	private boolean hasBlock;
	private boolean hasDust;
	
	private Resources(String name, int meta, boolean hasOreId, boolean hasBlock, boolean hasDust)
	{
		this.name = name;
		this.meta = meta;
		this.hasOreId = hasOreId;
		this.hasBlock = hasBlock;
		this.hasDust = hasDust;
	}
	
	private Resources(String name, int meta)
	{
		this(name, meta, true, true, true);
	}
	
	public int getMeta()
	{
		return meta;
	}
	
	public String getName()
	{
		return name;
	}

	public boolean hasOreId()
	{
		return hasOreId;
	}

	public boolean hasBlock()
	{
		return hasBlock;
	}

	public boolean hasDust()
	{
		return hasDust;
	}

	public @Nullable String getOreName(String prefix)
	{		
		return String.format("%s%s", prefix, getName());
	}
}
