/**
 * 
 */
package sfaxx.refinedtweaks.references;

public final class GuiIDs
{
	private GuiIDs()
	{
		
	}
	
	public static final int XP_STORAGE = 0;
	public static final int DISENCHANTER = 1;
	public static final int VOIDER = 2;
}
