package sfaxx.refinedtweaks.references;

public class Links
{
    public  static final String NAME = "Refined Tweaks";
    public  static final String MODID = "refinedtweaks";
    public  static final String VERSION = "@VERSION@";
    public  static final String DEPENDENCIES = "required-after:Forge@[12.18.2.2117,);required-after:immersiveengineering@[0.10-50,)";
    public  static final String GUI_FACTORY = "sfaxx.refinedtweaks.client.gui.config.RTGuiFactory";
    
    public  static final String WAILA_MODID = "Waila";
    public  static final String TOP_MODID = "theoneprobe";
    public  static final String RS_MODID = "refinedstorage";
    public  static final String IE_MODID = "immersiveengineering";
    public  static final String BOTANIA_MODID = "Botania";
    public  static final String EIO_MODID = "EnderIO";
    public  static final String BM_MODID = "BloodMagic";
    public  static final String SIMPLY_JETPACKS_MODID = "simplyjetpacks";
    public  static final String RFTOOLS_MODID = "rftools";
    public  static final String RFTOOLS_CONTROL_MODID = "rftoolscontrol";
    public  static final String THERMAL_EXPANSION_MODID = "thermalexpansion";
    public  static final String MFR_MODID = "minefactoryreloaded";

    public  static final String PROXY_CLIENT = "sfaxx.refinedtweaks.proxy.ClientProxy";
    public  static final String PROXY_SERVER = "sfaxx.refinedtweaks.proxy.CommonProxy";
}
